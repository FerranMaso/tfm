﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class Helper
{
	public static Vector3 IsometricToReal(Vector3 iso, Vector3 desp)
	{
		float xIso = iso.x;
		float yIso = iso.y;

		float xReal = (xIso * 0.5f) + (yIso * (-0.5f)) + desp.x;
		float yReal = (xIso * 0.25f) + (yIso * 0.25f) + desp.y;

		return new Vector3(xReal, yReal, 0);
	}

	public static Vector3 RealToIsometric(Vector3 real, Vector3 desp, bool arr)
	{
		float xReal = real.x;
		float yReal = real.y;

		float xIso = xReal + 2 * yReal - desp.x - 2 * desp.y;
		float yIso = -xReal + 2 * yReal + desp.x - 2 * desp.y;
		if(arr)
		{
			return new Vector3((int)xIso, (int)yIso, 0);
		}
		else
		{
			return new Vector3(xIso, yIso, 0);
		}
	}


}
