﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

	public enum WalkDirection
	{
		idle,
		thinkDirection,
		up,
		down,
		left,
		right
	}


	public Transform[] positions;
	private int actualPosition;

	private float waitTime = -1f;
	private float acumWaitTime = 0f;
	public float maxWaitTime = 8f;

	List<Node> pathNodes;
	public Transform player;
	public TilemapNavigation tmn;

	public Animator playerAnimator;

	[Tooltip("Make sense when walk mode is 'smooth', suggest 0 to 10")]
	public float smoothMoveSpeed = 2f;

	public WalkDirection walkDirec = WalkDirection.idle;

	// Use this for initialization
	void Start()
	{
		actualPosition = 0;
		pathNodes = new List<Node>();
	}

	// Update is called once per frame
	void Update()
	{
		if (walkDirec == WalkDirection.idle)
		{
			if (pathNodes.Count > 0)
			{
				if (acumWaitTime < waitTime)
				{
					acumWaitTime += Time.deltaTime;
				}
				else
				{
					walkDirec = WalkDirection.thinkDirection;
					StartCoroutine(SmoothMovePlayer());
					acumWaitTime = 0f;
				}

			}
			else
			{
				//Debug.Log(new Vector3(player.transform.position.x, player.position.y, 0));
				Vector3 isometricPositionPlayer = Helper.RealToIsometric(new Vector3(player.transform.position.x, player.position.y, 0), new Vector3(0, 0.25f, 0), true) - new Vector3(0, 1, 0);
				Debug.Log(isometricPositionPlayer);

				//Debug.Log(new Vector3(positions[actualPosition].position.x, positions[actualPosition].position.y, 0));
				Vector3 isometricPositionDestination = Helper.RealToIsometric(new Vector3(positions[actualPosition].position.x, positions[actualPosition].position.y, 0), new Vector3(0, 0.25f, 0), true);
				//Debug.Log(isometricPositionDestination);
				pathNodes = tmn.GetShortestPathAstar(new Vector3Int((int)isometricPositionPlayer.x, (int)isometricPositionPlayer.y, 0),
											   		 new Vector3Int((int)isometricPositionDestination.x, (int)isometricPositionDestination.y, 0));
				/*pathNodes = tmn.GetShortestPathAstar(new Vector3Int((int)(player.transform.position.x - tmn.tilemapWalkable.tileAnchor.x), 
																	(int)(player.position.y - tmn.tilemapWalkable.tileAnchor.y), 0),
											   		 new Vector3Int((int)positions[actualPosition].position.x, 
													 				(int)positions[actualPosition].position.y, 0));*/
				Node prevNode = null;
				foreach (Node n in pathNodes)
				{
					if (prevNode != null)
					{
						Vector3 startPos = Helper.RealToIsometric(new Vector3(n.pos.x, n.pos.y, 0), new Vector3(0, 0.25f, 0), true);
						Vector3 finishPos = Helper.RealToIsometric(new Vector3(prevNode.pos.x, prevNode.pos.y, 0), new Vector3(0, 0.25f, 0), true);
						Debug.DrawRay(startPos, finishPos, Color.red, 1000);
					}
					prevNode = n;
				}
				if (actualPosition + 1 >= positions.Length) actualPosition = 0;
				else actualPosition++;
				waitTime = Random.Range(0f, maxWaitTime);
			}
		}
	}



	IEnumerator SmoothMovePlayer()
	{
		for (int i = 0, max = pathNodes.Count; i < max; i++)
		{
			bool isOver = false;
			while (!isOver)
			{
				Vector3 isometricPositionPlayer = Helper.RealToIsometric(new Vector3(player.transform.position.x, player.position.y, 0), new Vector3(0, 0.25f, 0), false);

				Debug.Log("IsometricPlayerPosition:" + isometricPositionPlayer);
				Debug.Log("Destination point " + i + ": " + pathNodes[i].pos);
				Vector3 offSet = new Vector3(pathNodes[i].pos.x, pathNodes[i].pos.y, 0) - isometricPositionPlayer;
				if (offSet.y > 0)
				{
					playerAnimator.SetBool("Stop", false);
					playerAnimator.SetInteger("WalkToPos", 3);
					walkDirec = WalkDirection.up;

				}
				else if (offSet.y < 0)
				{
					playerAnimator.SetBool("Stop", false);
					playerAnimator.SetInteger("WalkToPos", 1);
					walkDirec = WalkDirection.down;
				}
				else if (offSet.x < 0)
				{
					playerAnimator.SetInteger("WalkToPos", 2);

					playerAnimator.SetBool("Stop", false);

					walkDirec = WalkDirection.left;
				}
				else if (offSet.x > 0)
				{
					playerAnimator.SetInteger("WalkToPos", 4);
					playerAnimator.SetBool("Stop", false);
					walkDirec = WalkDirection.right;
				}


				Vector3 isometricDesp = offSet.normalized * smoothMoveSpeed * Time.deltaTime;
				Debug.Log("Isometric desplace: " + isometricDesp);
				player.position = Helper.IsometricToReal(isometricPositionPlayer + isometricDesp, new Vector3(0, 0.25f, 0));
				Vector3 realDestinationPoint = Helper.IsometricToReal(pathNodes[i].pos, new Vector3(0, 0.25f, 0));
				isometricPositionPlayer = Helper.RealToIsometric(new Vector3(player.transform.position.x, player.position.y, 0), new Vector3(0, 0.25f, 0), false);
				Debug.Log("IsometricPlayerPosition after desp:" + isometricPositionPlayer);
				if (Vector2.Distance(new Vector2(realDestinationPoint.x, realDestinationPoint.y), new Vector2(player.position.x, player.position.y)) < 0.03f)
				{

					Debug.Log("Change destination");
					isOver = true;
					Vector3 isometricPositionFinalDestination = new Vector3(pathNodes[i].pos.x, pathNodes[i].pos.y, 0);
					Vector3 realPositionFinalDestination = Helper.IsometricToReal(isometricPositionFinalDestination, new Vector3(0, 0.25f, 0));
					player.position = realPositionFinalDestination;
				}
				yield return new WaitForFixedUpdate();
			}
		}
		playerAnimator.SetBool("Stop", true);
		pathNodes.Clear();
		walkDirec = WalkDirection.idle;
	}
}
