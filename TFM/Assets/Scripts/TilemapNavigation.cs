﻿using UnityEngine;
using UnityEngine.Tilemaps;
using System.Collections.Generic;

public class TilemapNavigation : MonoBehaviour
{
	public Tilemap tilemapWalkable;
	TileBase[] allTiles;

	const int cellSize = 1;

	Node usefullTiles;

	List<Node> navigationNodes;

	void Start()
	{
		navigationNodes = new List<Node>();

		foreach (Vector3Int pos in tilemapWalkable.cellBounds.allPositionsWithin)
		{
			var localPlace = new Vector3Int(pos.x, pos.y, pos.z);
			TileBase tb = tilemapWalkable.GetTile(localPlace);
			if (tb != null && tb.name != "")
			{
				if (tilemapWalkable.GetColliderType(localPlace) == Tile.ColliderType.None)
				{
					Node n = new Node(new Vector3Int(pos.x, pos.y, 0), new List<Node.Connection>());
					//Debug.Log(localPlace.x + " " + localPlace.y + " " + localPlace.z + ":" + tb.name);
					int xNeight = (int)(pos.x - cellSize);
					int yNeight = (int)(pos.y - cellSize);


					if (tilemapWalkable.GetColliderType(new Vector3Int(xNeight, pos.y, pos.z)) == Tile.ColliderType.None)
					{
						Node destX = Node.SearchNode(new Vector3Int(xNeight, pos.y, 0), navigationNodes);
						if (destX != null)
						{
							Node.Connection c = new Node.Connection { destination = destX, cost = 1 };
							n.destinations.Add(c);
							Node.Connection c2 = new Node.Connection { destination = n, cost = 1 };
							destX.destinations.Add(c2);
							//Debug.DrawRay(new Vector3(pos.x + tilemapWalkable.tileAnchor.x, pos.y + tilemapWalkable.tileAnchor.y, 0), new Vector3(xNeight - pos.x, 0, 0), Color.blue, 1000);
							Vector3 realPoints = Helper.IsometricToReal(new Vector3(pos.x, pos.y, pos.z), new Vector3(0f, 0.25f, 0));
							Vector3 neightPoints = Helper.IsometricToReal(new Vector3(xNeight, pos.y, 0), new Vector3(0f, 0.25f, 0));
							Debug.DrawRay(new Vector3(realPoints.x, realPoints.y, 0), new Vector3(neightPoints.x - realPoints.x, neightPoints.y - realPoints.y, 0), Color.blue, 1000);
							Debug.Log("Draw Ray from: (" + pos.x + " " + pos.y + "),(" + realPoints.x + " " + realPoints.y + ") to: (" + xNeight + " " + pos.y + "), (" +neightPoints.x + ", " + neightPoints.y + ")");
						}
					}

					if (tilemapWalkable.GetColliderType(new Vector3Int(pos.x, yNeight, 0)) == Tile.ColliderType.None)
					{
						Node destY = Node.SearchNode(new Vector3Int(pos.x, yNeight, 0), navigationNodes);
						if (destY != null)
						{
							Node.Connection c = new Node.Connection { destination = destY, cost = 1 };
							n.destinations.Add(c);
							Node.Connection c2 = new Node.Connection { destination = n, cost = 1 };
							destY.destinations.Add(c2);
							//Debug.DrawRay(new Vector3(pos.x + tilemapWalkable.tileAnchor.x, pos.y + tilemapWalkable.tileAnchor.y, 0), new Vector3(0, yNeight - pos.y, 0), Color.blue, 1000);
							Vector3 realPoints = Helper.IsometricToReal(new Vector3(pos.x, pos.y, pos.z), new Vector3(0, 0.25f, 0));
							Vector3 neightPoints = Helper.IsometricToReal(new Vector3(pos.x, yNeight, 0), new Vector3(0, 0.25f, 0));
							Debug.DrawRay(new Vector3(realPoints.x, realPoints.y, 0), new Vector3(neightPoints.x - realPoints.x, neightPoints.y - realPoints.y, 0), Color.blue, 1000);
							Debug.Log("Draw Ray from: (" + pos.x + " " + pos.y + "),(" + realPoints.x + " " + realPoints.y + ") to: (" + pos.x + " " +yNeight + "), (" +neightPoints.x + ", " + neightPoints.y + ")");
						}
					}
					navigationNodes.Add(n);
				}
			}

		}

	}

	private void Update()
	{
		//DrawRay();
	}

	public void DrawRay()
	{
		foreach(Node n in navigationNodes) {
			foreach(Node.Connection c in n.destinations) {
				Vector3 realPoints = Helper.IsometricToReal(new Vector3(n.pos.x, n.pos.y, n.pos.z), new Vector3(0, 0, 0));
				//Debug.Log("Draw Ray from: " + n.pos.x + " " + n.pos.y + " to: " + c.destination.pos.x + " " + c.destination.pos.y);
				Debug.Log("Draw Ray from: " + n.pos.x + " " + n.pos.y + " to: " + c.destination.pos.x + " " + c.destination.pos.y);
			}
		}
	}

	public List<Node> GetNavigationNodes(Vector3Int actualPosition, Vector3Int destinationPosition)
	{
		List<Node> navigationNodes = new List<Node>();
		//navigationNodes.Add(SearchNode(actualPosition));
		//navigationNodes.Add(SearchNode(destinationPosition));



		return navigationNodes;
	}



	public List<Node> GetShortestPathAstar(Vector3Int actualPosition, Vector3Int destinationPosition)
	{
		Node firstNode = Node.SearchNode(actualPosition, navigationNodes);
		Node lastNode = Node.SearchNode(destinationPosition, navigationNodes);
		/*foreach (Node node in nodes)
			node.StraightLineDistanceToEnd = node.StraightLineDistanceTo(lastNode);*/
		Node node = new Node(firstNode, new List<Node>(), lastNode);
		node.maxCost = 0;
		return AstarSearch(node);
	}

	private List<Node> GetMinPath(List<Node> l, Node start)
	{
		List<Node> retList = new List<Node>();
		Node n = l[l.Count - 1];

		while (n != start && n != null)
		{
			retList.Add(n);
			Node m = null;
			foreach (Node.Connection c in n.destinations)
			{
				if (c.used)
				{
					if (m == null) m = c.destination;
					else if (m.maxCost > c.destination.maxCost) m = c.destination;
				}
			}
			n = m;
		}
		if (n != null) retList.Add(n);
		retList.Reverse();
		return retList;

	}

	private List<Node> AstarSearch(Node start)
	{
		List<Node> visited = new List<Node>();
		List<Node> queue = new List<Node>();

		queue.Add(start);
		bool found = false;
		while (queue.Count > 0 && !found)
		{
			Node n = null; int i = 0; int pos = 0;
			foreach (Node m in queue)
			{
				if (n == null) n = m;
				else
				{
					if (n.maxCost + n.StraightLineDistanceToEnd > m.maxCost + m.StraightLineDistanceToEnd)
					{
						n = m; pos = i;
					}
				}
				i++;
			}
			if (n.StraightLineDistanceToEnd == 0.0f)
			{
				found = true;
				visited.Add(n);
				return GetMinPath(visited, start);
			}

			foreach (Node.Connection child in n.destinations)
			{
				if (Node.SearchNode(child.destination.pos, visited) == null && Node.SearchNode(child.destination.pos, queue) == null)
				{
					child.destination.maxCost = n.maxCost + child.cost;
					foreach (Node.Connection child2 in child.destination.destinations)
					{
						if (child2.destination.pos == n.pos)
						{
							child2.used = true;
						}
					}
					queue.Add(child.destination);
				}
			}

			visited.Add(n);
			queue.RemoveAt(pos);

		}

		return new List<Node>();
		/*
		if (start.StraightLineDistanceToEnd > 0)
		{
			List<Node.Connection> listConnection = start.GetLessDestinationNotVisited(acum_cost);
			foreach(Node.Connection n in listConnection) {
				List<Node> returnListNode = AstarSearch(n.destination, acum_cost + n.cost);
				if(returnListNode != null) {
					returnListNode.Insert(0, n.destination);
					return returnListNode;
				}
			}
			return null;
		}
		else {
			List<Node> nodeList = new List<Node>();
			nodeList.Add(start);
			return nodeList;
		}*/
	}
}

