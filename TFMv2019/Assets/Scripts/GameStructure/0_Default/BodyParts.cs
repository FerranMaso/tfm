﻿public class BodyPartsConstants 
{
	public static int HAIR = 0;
	public static int HEAD = 1;
	public static int BODY = 2;
	public static int SHIRT = 3;
	public static int LEGS = 4;
	public static int TROUSERS = 5;
	public static int FOOT = 6;

	public static string[] BODY_PARTS = { "Hair", "Head", "Body", "Shirt", "Legs", "Trousers", "Foot" };
	public static string[] BODY_RELATION_SINGLE_PARTS = { "Hair", "Head", "Body", "Body", "Legs", "Legs", "Foot" };
	public static string[] SINGLE_PARTS = { "head", "body",  "legs"};

}
