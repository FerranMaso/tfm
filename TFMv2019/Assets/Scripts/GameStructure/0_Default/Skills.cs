﻿public class Skills
{
	public static int DEVELOPMENT = 1;
	public static int TYPING_SPEED = 2;
	public static int DESIGN = 3;
	public static int SOCIAL = 4;
	public static int LEARNING = 5;
	public static int FITNESS = 6;
	public static int MANAGEMENT = 7;
	public static int ORGANIZATION = 8;
	public static int ACCOUNTING = 9;
	public static int INTELIGENCE = 10;

	public static int initialSkillLevel = 25;


	public static string[] skillsName = new string[] { "Programación", "Velocidad tecleo", "Diseño", "Social", "Aprendizaje", "Atlético", "Liderazgo", "Organización", "Contabilidad", "Inteligencia" };
}
