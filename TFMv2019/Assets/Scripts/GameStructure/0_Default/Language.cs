﻿//this class is not used, we work directly with the languages posted in the projects

public class Language
{
	public static string CPLUSPLUS = "C++";
	public static string C = "C";
	public static string JAVA = "Java";
	public static string CSHARP = "C#";
	public static string DELPHI = "Delphi";
	public static string PYTHON = "Python";
	public static string SWIFT = "Swift";
	public static string RUBY = "Ruby";
	public static string COBOL = "Cobol";
	public static string[] Languages = new string[] {"C++", "C", "Java", "C#", "Delphi", "Python", "Swift", "Ruby", "Cobol"};


	//WEB BASED
	public static string JAVASCRIPT = "JavaScript";
	public static string PHP = "PHP";
	public static string HTML = "HTML";
	public static string RUBYONRAILS = "Ruby on Rails";
	public static string CSS = "CSS";
	public static string[] WebBased = new string[] { "JavaScript", "PHP", "HTML", "Ruby on Rails", "CSS" };

	//DATABASE
	public static string MYSQL = "MySQL";
	public static string MONGODB = "MongoDB";
	public static string POSTGRESQL = "PostgreSQL";
	public static string SQLSERVER = "SqlServer";
	public static string[] Database = new string[] { "MySQL", "MongoDB", "PostgreSQL", "SqlServer" };

	public static string[] AllLanguages = new string[] { "C++", "C", "Java", "C#", "Delphi", "Python", "Swift", "Ruby", "Cobol", "JavaScript", "PHP", "HTML", "Ruby on Rails", "CSS", "MySQL", "MongoDB", "PostgreSQL", "SqlServer" };
}
