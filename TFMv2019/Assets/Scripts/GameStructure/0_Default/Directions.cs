﻿public class Directions
{
	public enum Direction
	{
		idle,
		thinkDirection,
		FrontRight,
		FrontLeft,
		BackLeft,
		BackRight
	}

	public const int FRONT_RIGHT = 1;
	public const int FRONT_LEFT = 2;
	public const int BACK_LEFT = 3;
	public const int BACK_RIGHT = 4;

	public static string[] DirectionsName = { "FrontRight", "FrontLeft", "BackLeft", "BackRight" };

}
