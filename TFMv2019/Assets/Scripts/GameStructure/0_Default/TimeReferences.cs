﻿public class TimeReferences
{
	public static string[] DaysAbr = { "LU", "MA", "MI", "JU", "VI", "SA", "DO" };
	public static string[] Days = { "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado", "Domingo" };
	public static int daysNumber = 7;

	public static string[] MonthsAbr = { "ENE", "FEB", "MAR", "ABR", "MAY", "JUN", "JUL", "AGO", "SEP", "OCT", "NOV", "DIC" };
	public static string[] Months = { "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" };
	public static int monthsNumber = 12;
}
