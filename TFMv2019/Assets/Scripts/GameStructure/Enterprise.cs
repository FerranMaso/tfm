﻿using System;

[Serializable]
public class Enterprise 
{
	//game properties
	public int enterpriseId;
	public bool active;

	//enterprise global

	public string enterpriseName;
	public string enterpriseType; //for the moment we only can be freelance


	//economy and status
	public float currentMoney;
	public int reputation; //from 1 - 100
	public int popularity; //from 1 - 100
	public float administrativeCosts;
	public float laboralRisks; //only if you have workers, billed annualy
	internal float taxes;
	internal float licenses;
	//include taxes every 3 months
	//include developer licences
	public Loan[] loans;

	//enterprise containers

	public Office office;
	public Character[] workers;
	public Character[] proposalWorkers;
	public ProjectsList projects;

	//time properties
	public int secondsPlayed;

	public int foundationDay;
	public int foundationMonth;
	public int foundationYear;

	public int actualDay;
	public int actualMonth;
	public int actualYear;

	public int savedDay;
	public int savedMonth;
	public int savedYear;
	public int savedHour;
	public int savedMinute;

	public int lastIdWorker;
}
