﻿using System;
using UnityEngine;

[Serializable]
public class CharacterSkill
{
	public int idSkill;
	public string nameSkill;
	public int level;
	public int maxLevel;

}
