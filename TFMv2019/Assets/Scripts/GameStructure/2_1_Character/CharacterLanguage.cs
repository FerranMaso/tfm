﻿using System;

[Serializable]
public class CharacterLanguage
{
	public string languageName;
	public int knowledge; //from 0 to 100
}
