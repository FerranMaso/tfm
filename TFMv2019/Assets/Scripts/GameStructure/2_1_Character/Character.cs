﻿using System;

[Serializable]
public class Character
{
	public int idWorker;
	public float smoothMoveSpeed;
	public BodyParts[] bodyParts;
	public CharacterSkill[] skills;
	public CharacterLanguage[] languages;
	public float salary;
	public float insurance;
	public float cotization;
	public string nameCharacter;
	public string motivation;

	//starting day
	public int startDay;
	public int startMonth;
	public int startYear;

	public int stress;
	public int hungry;
	public int energy;
	public int happiness;

	public float xPosition;
	public float yPosition;
}
