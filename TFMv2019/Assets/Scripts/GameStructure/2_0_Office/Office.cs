﻿using System;

[Serializable]
public class Office
{
	public float monthlyCost;
	public float internetCost;
	public float basicElectricityCost;
	public float insuranceCost;

	public int happinessEffect; //0-10 the influence in the happiness of workers 
	public EquipmentObject[] equipmentObjects;
}
