﻿using System;

[Serializable]
public class EquipmentObject 
{
	public string equipmentObjectName;
	public decimal monthlyCost;
	public decimal monthsLife;
	public decimal buyPrice;
	public CharacterSkill[] skillsImprovement;
	public int positionX;
	public int positionY;
}
