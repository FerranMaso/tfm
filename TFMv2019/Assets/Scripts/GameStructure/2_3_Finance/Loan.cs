﻿using System;

[Serializable]
public class Loan
{
	public DateTime loanExecutionDate;
	public int remainingMonths;
	public DateTime loadFinalization;
	public float loanAmount;
	public float loanInterests;
	public float monthlyPaymentQuantity;
	public float insurancePayment;
}
