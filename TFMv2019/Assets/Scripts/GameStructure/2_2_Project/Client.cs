﻿using System;

[Serializable]
public class Client
{
    public string clientName;
	public int satisfaction; //From 0-10. The level of satisfaction of this client.
	public int reputation; //From 0-10. The reputation of that client.
	public int economyLevel; //From 0-10. The economy level of the client.
	public int greedy; //From 0-10. A level of 0 the client will pay a lot for the projects.
	public int strict; //From 0-10. The level of how strict is the client with the work.
	public int kidness; //From 0-10. The level of how pleasent is the client.
	public int knowledge; //From 0-10. The level of consciousness about the project.
}
