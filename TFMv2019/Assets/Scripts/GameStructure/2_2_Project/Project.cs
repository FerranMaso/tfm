﻿using System;

[Serializable]
public class Project
{
	public int idProject;

    public int deliverDay;
	public int deliverMonth;
	public int deliverYear;

	public int estimatedTimeDays;

	//public Client client;
	public string title;
	public string description;
	public float money;
	public float status;
	public float quality;


	public ProjectLanguage[] languages;
}
