﻿using System;

[Serializable]
public class ProjectLanguage
{
	public string languageName;
	public int percentageUse;
}