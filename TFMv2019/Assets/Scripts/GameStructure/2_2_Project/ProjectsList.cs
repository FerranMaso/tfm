﻿using System;

[Serializable]
public class ProjectsList
{
	public Project[] proposalProjects;
	public Project[] doingProjects;
	public Project[] doneProjects;
}
