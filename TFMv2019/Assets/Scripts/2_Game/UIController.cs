﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActualProjectTime
{
	public int idProject;
	public float time;
}

public class UIController : MonoBehaviour
{
	[Header("Top Menu")]
	public GameObject topMenu;
	public Text dateText;
	public Text moneyText;

	[Header("Bottom Menu")]
	public GameObject bottomMenu;

	[Header("Generic")]
	public GameObject panelSection;
	public GameObject genericPanel;
	public GameObject genericAcceptRefusePanel;

	[Header("Project")]
	public GameObject projectsPanel;
	public GameObject proposalProject;
	public GameObject doingProject;
	public GameObject doneProject;

	[Header("Current projects")]
	public GameObject currentProjectPrefab;
	public GameObject currentProjectsPanel;
	public Sprite doubleDown;
	public Sprite down;
	public Sprite middle;
	public Sprite up;
	public Sprite doubleUp;
	public List<ActualProjectTime> currentArrowTimes;
	public float timeDisplayArrowTime = 3f;

	[Header("Worker")]
	public GameObject workersPanel;
	public GameObject proposalWorker;
	public GameObject actualWorker;

	[Header("Finance")]
	public GameObject financePanel;
	public GameObject contentFinance;

	[Header("Sounds")]
	public AudioSource click;
	public AudioSource assignation;
	public AudioSource projectUp;
	public AudioSource projectDown;

	private Core core;

	private GameObject currentPanel;
	private GameObject currentButton;
	private GameObject currentButtonSub;

	private const int timeDisplayAutoSave = 4; // in seconds



	// Start is called before the first frame update
	void Start()
    {
		core = GetComponent<Core>();
		currentPanel = null;
		currentButton = null;
		currentButtonSub = null;
		//open project panel by default
		//OpenProjectsPanel();
		OpenCurrentTopPanelProjects();
		currentArrowTimes = new List<ActualProjectTime>();
		click.volume = LevelManager.sound / 100f;
		assignation.volume = LevelManager.sound / 100f; 
	    projectUp.volume = LevelManager.sound / 100f; 
		projectDown.volume = LevelManager.sound / 100f; 
}

    // Update is called once per frame
    void Update()
    {
		if(currentArrowTimes == null) 
			currentArrowTimes = new List<ActualProjectTime>();
		foreach (ActualProjectTime apt in currentArrowTimes)
		{
			if (apt.time > -1)
			{
				apt.time += Time.deltaTime;
				if (apt.time >= timeDisplayArrowTime)
				{
					apt.time = -1;
					RemoveCurrentArrow(apt.idProject);
				}
			}
		}
        
    }

	public void ClosePopUp()
	{
		if (currentPanel != null) currentPanel.SetActive(false);
		currentPanel = null;
		RemoveTransparencyCurrentButton();
		RemoveTransparencyCurrentButtonSub();
	}


	//PROJECT UI

	public void OpenProjectsPanel()
	{
		if (currentPanel != null) currentPanel.SetActive(false);
		projectsPanel.SetActive(true);
		currentPanel = projectsPanel;
		OpenDoingProjects();
		click.Play();
		SetTransparencyCurrentButton("ProjectsButton");
	}

	public void SetTransparencyCurrentButton(string buttonString)
	{
		RemoveTransparencyCurrentButton();
		currentButton = bottomMenu.transform.Find("Buttons").transform.Find(buttonString).gameObject;
		RawImage currentButtonImage = currentButton.GetComponent<RawImage>();
		Color tempColor = currentButtonImage.color;
		tempColor.a = 0.6f;
		currentButton.GetComponent<RawImage>().color = tempColor;
	}

	public void SetTransparencySubCurrentButton(string buttonString)
	{
		RemoveTransparencyCurrentButtonSub();
		currentButtonSub = Helper.FindDeepChild(panelSection.transform, buttonString).gameObject;
		Image currentButtonImage = currentButtonSub.GetComponent<Image>();
		Color tempColor = currentButtonImage.color;
		tempColor.a = 0.4f;
		currentButtonSub.GetComponent<Image>().color = tempColor;
	}

	public void RemoveTransparencyCurrentButton()
	{
		if(currentButton != null)
		{
			RawImage currentButtonImage = currentButton.GetComponent<RawImage>();
			Color tempColor = currentButtonImage.color;
			tempColor.a = 1f;
			currentButton.GetComponent<RawImage>().color = tempColor;
			currentButton = null;
		}
	}

	public void RemoveTransparencyCurrentButtonSub()
	{
		if (currentButtonSub != null)
		{
			Image currentButtonImage = currentButtonSub.GetComponent<Image>();
			Color tempColor = currentButtonImage.color;
			tempColor.a = 1f;
			currentButtonSub.GetComponent<Image>().color = tempColor;
			currentButtonSub = null;
		}
	}

	public void OpenProposalProjects()
	{
		click.Play();
		projectsPanel.SetActive(true);
		SetTransparencySubCurrentButton("PendingProjectsButton");
		Transform content = Helper.FindDeepChild(projectsPanel.transform, "Content");
		if (content != null)
		{
			foreach (Transform child in content)
			{
				GameObject.Destroy(child.gameObject);
			}
			Project[] proposalProjects = core.GetProposalProjects();
			for (int i = proposalProjects.Length - 1; i >= 0; i--) //invert the order of view
			{
				if (proposalProjects[i] != null)
				{
					GameObject project = Instantiate(proposalProject, content);
					project.transform.Find("ProjectProperties").Find("Title").GetComponent<Text>().text = proposalProjects[i].title;
					project.transform.Find("ProjectProperties").Find("Description").GetComponent<Text>().text = proposalProjects[i].description;
					string languages = ""; int j = 0;
					foreach (ProjectLanguage l in proposalProjects[i].languages)
					{
						j++;
						languages += l.languageName + " " + l.percentageUse + "%";
						if (j < proposalProjects[i].languages.Length)
						{
							languages += " - ";
						}
					}
					project.transform.Find("ProjectProperties").Find("Language").GetComponent<Text>().text = languages;
					project.transform.Find("ProjectProperties").Find("OtherProps").Find("Cost").GetComponent<Text>().text = proposalProjects[i].money.ToString("n2") + "€";
					project.transform.Find("ProjectProperties").Find("OtherProps").Find("Duration").GetComponent<Text>().text = proposalProjects[i].estimatedTimeDays + " días";
					int idProject = proposalProjects[i].idProject;
					project.transform.Find("ProjectProperties").Find("Buttons").Find("CancelButton").GetComponent<Button>().onClick.AddListener(() => { RefuseProject(idProject); });
					project.transform.Find("ProjectProperties").Find("Buttons").Find("ConfirmButton").GetComponent<Button>().onClick.AddListener(() => { AcceptProject(idProject); });
					//project.transform.Find("ProjectProperties").Find("Buttons").Find("ConfirmButton").Find("ConfirmButton");
				}
			}
		}
		else
		{
			throw new Exception("Content not found in projects");
		}
	}

	public void AcceptProject(int idProject)
	{
		assignation.Play();
		core.AcceptProposalProject(idProject);
		OpenProposalProjects();
		AddProject(idProject);
	}

	public void RefuseProject(int idProject)
	{
		projectDown.Play();
		core.RefuseProposalProject(idProject);
		OpenProposalProjects();
	}

	public void OpenDoingProjects()
	{
		click.Play();
		projectsPanel.SetActive(true);
		SetTransparencySubCurrentButton("DoingProjectsButton");
		Transform content = Helper.FindDeepChild(projectsPanel.transform, "Content");
		if (content != null)
		{
			foreach (Transform child in content)
			{
				GameObject.Destroy(child.gameObject);
			}
			Project[] doingProjects = core.GetDoingProjects();
			for (int i = doingProjects.Length - 1; i >= 0; i--) //invert the order of view
			{
				GameObject project = Instantiate(doingProject, content);
				project.transform.Find("ProjectProperties").Find("Title").GetComponent<Text>().text = doingProjects[i].title;
				project.transform.Find("ProjectProperties").Find("Description").GetComponent<Text>().text = doingProjects[i].description;
				string languages = ""; int j = 0;
				foreach (ProjectLanguage l in doingProjects[i].languages)
				{
					j++;
					languages += l.languageName + " " + l.percentageUse + "%";
					if (j < doingProjects[i].languages.Length)
					{
						languages += " - ";
					}
				}
				project.transform.Find("ProjectProperties").Find("Language").GetComponent<Text>().text = languages;
				project.transform.Find("ProjectProperties").Find("OtherProps").Find("Cost").GetComponent<Text>().text = doingProjects[i].money.ToString("n2") + "€";
				project.transform.Find("ProjectProperties").Find("OtherProps").Find("Duration").GetComponent<Text>().text = doingProjects[i].estimatedTimeDays + " días";
				int idProject = doingProjects[i].idProject;
				project.transform.Find("ProjectProperties").Find("Buttons").Find("CancelButton").GetComponent<Button>().onClick.AddListener(() => { CancelProjectPopUp(idProject); }); ;
			}
		}
		else
		{
			throw new Exception("Content not found in projects");
		}
	}

	public void CancelProjectPopUp(int idProject)
	{
		Debug.Log("Canceling idProject: " + idProject);
		Project proj = core.GetProjectInDoing(idProject);
		Enterprise ent = core.GetEnterprise();
		float moneyPenalization = proj.money * (float)Finance.PROJECT_CANCEL_PENALIZATION_MONEY / 100;
		int reputationPenalization = ent.reputation * (int)Finance.PROJECT_CANCEL_PENALIZATION_REPUTATION / 100;

		genericAcceptRefusePanel.SetActive(true);
		currentPanel.SetActive(false);
		currentPanel = genericAcceptRefusePanel;
		Helper.FindDeepChild(genericAcceptRefusePanel.transform, "Title").GetComponent<Text>().text = "Cancelar Proyecto";
		Helper.FindDeepChild(genericAcceptRefusePanel.transform, "Description").GetComponent<Text>().text = "Estás seguro que deseas cancelar el proyecto " + proj.title + "? \n\r" +
			"Penalización por cancelación: -" + moneyPenalization.ToString("n2") + "€ \r\n" +
			"Reputación de la empresa: -" + (int)reputationPenalization + "";
		int id = idProject;
		Helper.FindDeepChild(genericAcceptRefusePanel.transform, "CancelButton").GetComponent<Button>().onClick.RemoveAllListeners();
		Helper.FindDeepChild(genericAcceptRefusePanel.transform, "CancelButton").GetComponent<Button>().onClick.AddListener(() => { ClosePopUp(); });
		Helper.FindDeepChild(genericAcceptRefusePanel.transform, "ConfirmButton").GetComponent<Button>().onClick.RemoveAllListeners();
		Helper.FindDeepChild(genericAcceptRefusePanel.transform, "ConfirmButton").GetComponent<Button>().onClick.AddListener(() => { CancelProject(id); });
	}

	public void CancelProject(int idProject)
	{
		projectDown.Play();
		core.CancelProject(idProject);
		RemoveCurrentProject(idProject);
		ClosePopUp();
	}


	public void OpenDoneProjects()
	{
		click.Play();
		projectsPanel.SetActive(true);
		SetTransparencySubCurrentButton("DoneProjectsButton");
		Transform content = Helper.FindDeepChild(projectsPanel.transform, "Content");
		if (content != null)
		{
			foreach (Transform child in content)
			{
				GameObject.Destroy(child.gameObject);
			}
			Project[] doneProjects = core.GetDoneProjects();
			for (int i = doneProjects.Length - 1; i >= 0; i--) //invert the order of view
			{
				GameObject project = Instantiate(doneProject, content);
				project.transform.Find("ProjectProperties").Find("Title").GetComponent<Text>().text = doneProjects[i].title;
				project.transform.Find("ProjectProperties").Find("Description").GetComponent<Text>().text = doneProjects[i].description;
				string languages = ""; int j = 0;
				foreach (ProjectLanguage l in doneProjects[i].languages)
				{
					j++;
					languages += l.languageName + " " + l.percentageUse + "%";
					if (j < doneProjects[i].languages.Length)
					{
						languages += " - ";
					}
				}
				project.transform.Find("ProjectProperties").Find("Language").GetComponent<Text>().text = languages;
				project.transform.Find("ProjectProperties").Find("OtherProps").Find("Cost").GetComponent<Text>().text = doneProjects[i].money.ToString("n2") + "€";
				project.transform.Find("ProjectProperties").Find("OtherProps").Find("Duration").GetComponent<Text>().text = doneProjects[i].estimatedTimeDays + " días";
				//project.transform.Find("ProjectProperties").Find("Buttons").Find("CancelButton").Find("CancelButton");
			}
		}
		else
		{
			throw new Exception("Content not found in projects");
		}
	}

	private void LoadCurrentProjects()
	{
		projectsPanel.SetActive(true);
		Transform content = Helper.FindDeepChild(projectsPanel.transform, "Content");
		if (content != null)
		{
			foreach (Transform child in content)
			{
				GameObject.Destroy(child.gameObject);
			}
			Project[] doingProjects = core.GetDoingProjects();
			for (int i = doingProjects.Length - 1; i >= 0; i--) //invert the order of view
			{
				GameObject project = Instantiate(doingProject, content);
				project.transform.Find("ProjectProperties").Find("Title").GetComponent<Text>().text = doingProjects[i].title;
				project.transform.Find("ProjectProperties").Find("Description").GetComponent<Text>().text = doingProjects[i].description;
				string languages = ""; int j = 0;
				foreach (ProjectLanguage l in doingProjects[i].languages)
				{
					j++;
					languages += l.languageName + " " + l.percentageUse + "%";
					if (j < doingProjects[i].languages.Length)
					{
						languages += " - ";
					}
				}
				project.transform.Find("ProjectProperties").Find("Language").GetComponent<Text>().text = languages;
				project.transform.Find("ProjectProperties").Find("OtherProps").Find("Cost").GetComponent<Text>().text = doingProjects[i].money.ToString("n2") + "€";
				project.transform.Find("ProjectProperties").Find("OtherProps").Find("Duration").GetComponent<Text>().text = doingProjects[i].estimatedTimeDays + " días";
			}
		}
		else
		{
			throw new Exception("Content not found in projects");
		}
	}

	private void OpenCurrentTopPanelProjects()
	{
		click.Play();
		foreach (Transform child in currentProjectsPanel.transform)
		{
			GameObject.Destroy(child.gameObject);
		}

		Project[] doingProjects = core.GetDoingProjects();
		for (int i = 0; i < doingProjects.Length; i++) //invert the order of view
		{
			GameObject project = Instantiate(currentProjectPrefab, currentProjectsPanel.transform);
			project.name = "Project#" + doingProjects[i].idProject;
			project.transform.Find("RawImage").Find("ProjectTitle").GetComponent<Text>().text = doingProjects[i].title;
			project.transform.Find("Workers").GetComponent<Text>().text = "Integrantes: ";
			project.transform.Find("Properties").Find("Progress").GetComponent<Text>().text = "Progreso: " + doingProjects[i].status.ToString("n0") + "%";
			project.transform.Find("Properties").Find("Quality").GetComponent<Text>().text = "Calidad: " + doingProjects[i].quality.ToString("n0") + "%";
		}
	}

	private void AddProject(int idProject)
	{
		Project proj = core.GetProjectInDoing(idProject);
		GameObject project = Instantiate(currentProjectPrefab, currentProjectsPanel.transform);
		project.name = "Project#" + proj.idProject;
		project.transform.Find("RawImage").Find("ProjectTitle").GetComponent<Text>().text = proj.title;
		project.transform.Find("Workers").GetComponent<Text>().text = "Integrantes: ";
		project.transform.Find("Properties").Find("Progress").GetComponent<Text>().text = "Progreso: " + proj.status.ToString("n0") + "%";
		project.transform.Find("Properties").Find("Quality").GetComponent<Text>().text = "Calidad: " + proj.quality.ToString("n0") + "%";
	}

	public void RefreshCurrentProject(int idProject, int quality)
	{
		Project proj = core.GetProjectInDoing(idProject);
		Transform projectUI = currentProjectsPanel.transform.Find("Project#" + proj.idProject);
		projectUI.transform.Find("RawImage").Find("ProjectTitle").GetComponent<Text>().text = proj.title;
		projectUI.transform.Find("Workers").GetComponent<Text>().text = "Integrantes: ";
		projectUI.transform.Find("Properties").Find("Progress").GetComponent<Text>().text = "Progreso: " + proj.status.ToString("n0") + "%";
		projectUI.transform.Find("Properties").Find("Quality").GetComponent<Text>().text = "Calidad: " + proj.quality.ToString("n0") + "%";
		Transform stateImage = projectUI.transform.Find("Properties").Find("Quality").Find("PreviousState");
		Sprite projectArrow = null;
		switch (quality)
		{
			case 0:
				projectArrow = doubleDown;
				break;
			case 25:
				projectArrow = down;
				break;
			case 50:
				projectArrow = middle;
				break;
			case 75:
				projectArrow = up;
				break;
			case 100:
				projectArrow = doubleUp;
				break;
		}
		if(projectArrow!=null)
		{
			stateImage.gameObject.SetActive(true);
			stateImage.GetComponent<Image>().sprite = projectArrow;
			if (currentArrowTimes == null) currentArrowTimes = new List<ActualProjectTime>();
			bool found = false;
			foreach(ActualProjectTime apt in currentArrowTimes)
			{
				if(apt.idProject == proj.idProject)
				{
					apt.time = 0;
					found = true;
					break;
				}
			}
			if (!found)
			{
				currentArrowTimes.Add(new ActualProjectTime()
				{
					idProject = proj.idProject,
					time = 0
				});
			}
		}
	}

	private void RemoveCurrentArrow(int idproject)
	{
		Transform projectUI = currentProjectsPanel.transform.Find("Project#" + idproject);
		Transform stateImage = projectUI.transform.Find("Properties").Find("Quality").Find("PreviousState");
		stateImage.gameObject.SetActive(false);
	}

	public void RemoveCurrentProject(int idProject)
	{
		GameObject projectUI = currentProjectsPanel.transform.Find("Project#" + idProject).gameObject;
		GameObject.Destroy(projectUI);
		currentArrowTimes.RemoveAll(p => p.idProject == idProject);
	}

	public void OpenGenericPanel(string title, string description)
	{
		ClosePopUp();
		currentPanel = genericPanel;
		Helper.FindDeepChild(genericPanel.transform, "Title").GetComponent<Text>().text = title;
		Helper.FindDeepChild(genericPanel.transform, "Description").GetComponent<Text>().text = description;
		genericPanel.SetActive(true);
	}

	internal void SetMoneyText(string money)
	{
		moneyText.text = money;
	}

	internal void SetDateText(string date)
	{
		dateText.text = date;
	}

	public void CloseGenericPanel()
	{
		RemoveTransparencyCurrentButton();
		RemoveTransparencyCurrentButtonSub();
		genericPanel.SetActive(false);
	}

	//Finance

	public void OpenFinancePanel()
	{
		click.Play();
		if (currentPanel != null)
		{
			currentPanel.SetActive(false);
		}

		financePanel.SetActive(true);
		currentPanel = financePanel;
		SetTransparencyCurrentButton("FinanceButton");

		Helper.FindDeepChild(contentFinance.transform, "EnterpriseSpecifications").transform.gameObject.SetActive(false);
		Helper.FindDeepChild(financePanel.transform, "MonthlyCosts").transform.gameObject.SetActive(false);
		Helper.FindDeepChild(financePanel.transform, "AnualCosts").transform.gameObject.SetActive(false);

		OpenEnterpriseSpecifications();

	}

	public void OpenEnterpriseFinancials()
	{
		click.Play();
		SetTransparencySubCurrentButton("EnterpriseFinancialsButton");
		//specificationsFinance.SetActive(false);

		Helper.FindDeepChild(contentFinance.transform, "EnterpriseSpecifications").transform.gameObject.SetActive(false);
		GameObject monthlyCostsPanel = Helper.FindDeepChild(financePanel.transform, "MonthlyCosts").transform.gameObject as GameObject;
		GameObject anualCostsPanel = Helper.FindDeepChild(financePanel.transform, "AnualCosts").transform.gameObject as GameObject;
		//GameObject workersPanel = Helper.FindDeepChild(financePanel.transform, "Workers").transform.gameObject as GameObject;
		//GameObject anualCostsPanel = Helper.FindDeepChild(financePanel.transform, "AnualCosts").transform.gameObject as GameObject;

		monthlyCostsPanel.SetActive(true);
		anualCostsPanel.SetActive(true);

		// Monthly costs panel
		Helper.FindDeepChild(monthlyCostsPanel.transform, "LocalCost").GetComponent<Text>().text = string.Format("Local: {0}€", core.enterprise.office.monthlyCost.ToString("n2"));
		Helper.FindDeepChild(monthlyCostsPanel.transform, "InternetCost").GetComponent<Text>().text = string.Format("Internet: {0}€", core.enterprise.office.internetCost.ToString("n2"));
		Helper.FindDeepChild(monthlyCostsPanel.transform, "ElectricityCost").GetComponent<Text>().text = string.Format("Eletricidad: {0}€", core.enterprise.office.basicElectricityCost.ToString("n2"));
		Helper.FindDeepChild(monthlyCostsPanel.transform, "InsuranceCost").GetComponent<Text>().text = string.Format("Seguros: {0}€", core.enterprise.office.insuranceCost.ToString("n2"));
		

		// Year costs panel
		Helper.FindDeepChild(anualCostsPanel.transform, "RiskCost").GetComponent<Text>().text = string.Format("Riesgos laborales: {0}€", core.enterprise.laboralRisks.ToString("n2"));
		Helper.FindDeepChild(anualCostsPanel.transform, "LicencesCost").GetComponent<Text>().text = string.Format("Licencias de soft: {0}€", core.enterprise.licenses.ToString("n2"));
		Helper.FindDeepChild(anualCostsPanel.transform, "TaxCost").GetComponent<Text>().text = string.Format("Impuestos: {0}€", core.enterprise.taxes.ToString("n2"));
		

		// Workers

		//GameObject workersContentPanel = contentFinance.transform.Find("Workers").gameObject as GameObject;
		//workersContentPanel.SetActive(true);

		float totalWorkersCost = 0;

		/*if (workersContentPanel.transform.childCount > 1)
		{
			foreach (Transform t in workersContentPanel.transform)
			{
				if (t.name.Equals("WorkerPanel(Clone)"))
				{
					Destroy(t.gameObject);
				}
			}
		}*/

		foreach (Character c in core.enterprise.workers)
		{

			//workerPrefab.transform.SetParent(workersContentPanel.transform);
			//workerPrefab.transform.Find("Top").transform.Find("WorkerName").GetComponent<Text>().text = string.Format("Nombre trabajador: {0}", c.nameCharacter);

			//workerPrefab.transform.Find("Bottom").transform.Find("WorkerCotization").GetComponent<Text>().text = string.Format(format, value);
			//workerPrefab.transform.Find("Bottom").transform.Find("WorkerInsurance").GetComponent<Text>().text = string.Format("Seguro trabajador: {0}", c.insurance);
			totalWorkersCost += c.salary + c.cotization + c.insurance;
		}

		/*workersContentPanel.transform.Find("TitleWorkersCost").transform.SetAsFirstSibling();

		workersContentPanel.transform.Find("TotalWorkersCost").GetComponent<Text>().text = string.Format("Total trabajadores: {0}", totalWorkersCost);
		workersContentPanel.transform.Find("TotalWorkersCost").transform.SetAsLastSibling();*/

		Helper.FindDeepChild(monthlyCostsPanel.transform, "WorkersCost").GetComponent<Text>().text = string.Format("Coste trabajadores: {0}€", totalWorkersCost);

		float totalYearCost = core.enterprise.laboralRisks + core.enterprise.licenses + core.enterprise.taxes;
		Helper.FindDeepChild(anualCostsPanel.transform, "TotalYearCost").GetComponent<Text>().text = string.Format("Total: {0}€", totalYearCost);

		float totalMonthlyCost = core.enterprise.office.monthlyCost + core.enterprise.office.internetCost + core.enterprise.office.basicElectricityCost + core.enterprise.office.insuranceCost + totalWorkersCost;
		Helper.FindDeepChild(monthlyCostsPanel.transform, "TotalMonthlyCost").GetComponent<Text>().text = string.Format("Total: {0}€", totalMonthlyCost);

		// Anual costs panel

	}

	public void OpenEnterpriseSpecifications()
	{
		click.Play();
		SetTransparencySubCurrentButton("EnterpriseSpecificationsButton");
		GameObject specPanel = Helper.FindDeepChild(contentFinance.transform, "EnterpriseSpecifications").transform.gameObject as GameObject;
		Helper.FindDeepChild(financePanel.transform, "MonthlyCosts").transform.gameObject.SetActive(false);
		Helper.FindDeepChild(financePanel.transform, "AnualCosts").transform.gameObject.SetActive(false);
		specPanel.SetActive(true);

		Helper.FindDeepChild(specPanel.transform, "SpecificationProperties").Find("Reputation")
				.GetComponent<Text>().text = string.Format("Reputacion: {0}/100", core.enterprise.reputation);

		Helper.FindDeepChild(specPanel.transform, "SpecificationProperties").Find("Popularity")
				.GetComponent<Text>().text = string.Format("Popularidad: {0}/100", core.enterprise.popularity);

		Helper.FindDeepChild(specPanel.transform, "SpecificationProperties").Find("Money")
				.GetComponent<Text>().text = string.Format("Dinero: {0}€", core.enterprise.currentMoney);

		Helper.FindDeepChild(specPanel.transform, "SpecificationProperties").Find("CompanyName")
				.GetComponent<Text>().text = string.Format("Nombre Empresa: {0}", core.enterprise.enterpriseName);

		string founderName = "";
		int index = 0;
		bool trobat = false;

		while (index < core.enterprise.workers.Length && !trobat)
		{
			Debug.Log(index + " " + core.enterprise.workers[index].salary);
			if (core.enterprise.workers[index].salary == 0)
			{
				founderName = core.enterprise.workers[index].nameCharacter;
				trobat = true;
			}
			index++;
		}

		Helper.FindDeepChild(specPanel.transform, "SpecificationProperties").Find("FoundationName")
				.GetComponent<Text>().text = string.Format("Nombre Fundador: {0}", founderName);

		Helper.FindDeepChild(specPanel.transform, "SpecificationProperties").Find("WorkersNumber")
				.GetComponent<Text>().text = string.Format("Número de trabajadores: {0}", core.enterprise.workers.Length);

		Helper.FindDeepChild(specPanel.transform, "SpecificationProperties").Find("FoundationDay")
				.GetComponent<Text>().text = string.Format("Data fundación: {0}/{1}/{2}", core.enterprise.foundationDay, core.enterprise.foundationMonth, core.enterprise.foundationYear);

		Helper.FindDeepChild(specPanel.transform, "SpecificationProperties").Find("ProjectsDone")
				.GetComponent<Text>().text = string.Format("Proyectos realizados: {0}", core.enterprise.projects.doneProjects.Length);

		DateTime foundationDate = new DateTime(core.enterprise.foundationYear, core.enterprise.foundationMonth, core.enterprise.foundationDay);
		DateTime actualDate = new DateTime(core.enterprise.actualYear, core.enterprise.actualMonth, core.enterprise.actualDay);

		Helper.FindDeepChild(specPanel.transform, "SpecificationProperties").Find("TimeActive")
				.GetComponent<Text>().text = string.Format("Tiempo activo: {0} años {1} meses", actualDate.Year - foundationDate.Year, ((actualDate.Month - foundationDate.Month) + 12 * (actualDate.Year - foundationDate.Year)) % 12);

	}

	// Workers panel
	public void OpenWorkersPanel()
	{
		if (currentPanel != null) currentPanel.SetActive(false);
		workersPanel.SetActive(true);
		currentPanel = workersPanel;
		SetTransparencyCurrentButton("WorkersButton");
		OpenCurrentWorkers();
	}

	public void OpenCurrentWorkers()
	{
		click.Play();
		workersPanel.SetActive(true);
		SetTransparencySubCurrentButton("CurrentButton");
		Transform content = Helper.FindDeepChild(workersPanel.transform, "Content");
		if (content != null)
		{
			foreach (Transform child in content)
			{
				GameObject.Destroy(child.gameObject);
			}
			Character[] currentCharacters = core.GetCurrentWorkers();
			for (int i = currentCharacters.Length - 1; i >= 0; i--) //invert the order of view
			{
				int idWorker = currentCharacters[i].idWorker;
				GameObject worker = Instantiate(actualWorker, content);
				worker.transform.Find("WorkerProperties").Find("Title").GetComponent<Text>().text = currentCharacters[i].nameCharacter;

				string skills = ""; int j = 0;
				foreach (CharacterSkill cs in currentCharacters[i].skills)
				{
					j++;
					skills += cs.nameSkill + " " + cs.level + "/" + cs.maxLevel;
					if (j < currentCharacters[i].skills.Length)
					{
						skills += "\r\n";
					}
				}
				worker.transform.Find("WorkerProperties").Find("Props").Find("Skills").GetComponent<Text>().text = skills;

				int k = 0;
				string languages = "";
				foreach (CharacterLanguage cl in currentCharacters[i].languages)
				{
					k++;
					languages += cl.languageName + " " + cl.knowledge + "%";
					if (k < currentCharacters[i].languages.Length)
					{
						languages += "\r\n";
					}
				}
				worker.transform.Find("WorkerProperties").Find("Props").Find("Languages").GetComponent<Text>().text = languages;

				if(idWorker == 1)
				{
					worker.transform.Find("WorkerProperties").Find("OtherProps").Find("Salary").GetComponent<Text>().text = "Seguridad social: " + currentCharacters[i].cotization.ToString("n2") + "€";
					worker.transform.Find("WorkerProperties").Find("Buttons").Find("CancelButton").gameObject.SetActive(false);
				}
				else
				{
					worker.transform.Find("WorkerProperties").Find("OtherProps").Find("Salary").GetComponent<Text>().text = "Salario: " + currentCharacters[i].salary.ToString("n2") + "€";
					worker.transform.Find("WorkerProperties").Find("Buttons").Find("CancelButton").GetComponent<Button>().onClick.AddListener(() => { FireWorker(idWorker); });
				}

				worker.transform.Find("WorkerProperties").Find("OtherProps").Find("Insurance").GetComponent<Text>().text = "Seguro: " + currentCharacters[i].insurance.ToString("n2") + "€";

				//todo: i'm here
				foreach(BodyParts bp in currentCharacters[i].bodyParts)
				{
					Debug.Log("Sprites/Character/FrontRight/" + BodyPartsConstants.BODY_RELATION_SINGLE_PARTS[bp.part] + "/" + bp.nameSprite + "_idle_0");
					Sprite[] sprites = Resources.LoadAll<Sprite>("Sprites/Characters/FrontRight/" + BodyPartsConstants.BODY_RELATION_SINGLE_PARTS[bp.part] + "/" + bp.nameSprite + "_idle");
					if(sprites.Length > 0) {
						worker.transform.Find("Worker").Find(BodyPartsConstants.BODY_PARTS[bp.part]).GetComponent<Image>().sprite = sprites[0];
						Color color;
						ColorUtility.TryParseHtmlString(bp.color, out color);
						worker.transform.Find("Worker").Find(BodyPartsConstants.BODY_PARTS[bp.part]).GetComponent<Image>().color = color;
					}
				}
			}
		}
		else
		{
			throw new Exception("Content not found in workers");
		}
	}

	public void OpenProposalWorkers()
	{
		click.Play();
		workersPanel.SetActive(true);
		SetTransparencySubCurrentButton("ContractButton");
		Transform content = Helper.FindDeepChild(workersPanel.transform, "Content");
		if (content != null)
		{
			foreach (Transform child in content)
			{
				GameObject.Destroy(child.gameObject);
			}
			Character[] currentCharacters = core.GetProposalWorkers();
			for (int i = currentCharacters.Length - 1; i >= 0; i--) //invert the order of view
			{
				int idWorker = currentCharacters[i].idWorker;
				GameObject worker = Instantiate(proposalWorker, content);
				worker.transform.Find("WorkerProperties").Find("Title").GetComponent<Text>().text = currentCharacters[i].nameCharacter;

				string skills = ""; int j = 0;
				foreach (CharacterSkill cs in currentCharacters[i].skills)
				{
					j++;
					skills += cs.nameSkill + " " + cs.level + "/" + cs.maxLevel;
					if (j <= currentCharacters[i].skills.Length)
					{
						skills += "\r\n";
					}
				}
				worker.transform.Find("WorkerProperties").Find("Props").Find("Skills").GetComponent<Text>().text = skills;

				int k = 0;
				string languages = "";
				foreach (CharacterLanguage cl in currentCharacters[i].languages)
				{
					k++;
					languages += cl.languageName + " " + cl.knowledge + "%";
					if (k <= currentCharacters[i].languages.Length)
					{
						languages += "\r\n";
					}
				}
				worker.transform.Find("WorkerProperties").Find("Props").Find("Languages").GetComponent<Text>().text = languages;

				worker.transform.Find("WorkerProperties").Find("OtherProps").Find("Salary").GetComponent<Text>().text = "Salario: " + currentCharacters[i].salary.ToString("n2") + "€";
				worker.transform.Find("WorkerProperties").Find("Buttons").Find("CancelButton").GetComponent<Button>().onClick.AddListener(() => { RefuseWorker(idWorker); });
				worker.transform.Find("WorkerProperties").Find("Buttons").Find("AcceptButton").GetComponent<Button>().onClick.AddListener(() => { HireWorker(idWorker); });
				

				worker.transform.Find("WorkerProperties").Find("OtherProps").Find("Insurance").GetComponent<Text>().text = "Seguro: " + currentCharacters[i].insurance.ToString("n2") + "€";

				//todo: i'm here
				foreach (BodyParts bp in currentCharacters[i].bodyParts)
				{
					Debug.Log("Sprites/Character/FrontRight/" + BodyPartsConstants.BODY_RELATION_SINGLE_PARTS[bp.part] + "/" + bp.nameSprite + "_idle_0");
					Sprite[] sprites = Resources.LoadAll<Sprite>("Sprites/Characters/FrontRight/" + BodyPartsConstants.BODY_RELATION_SINGLE_PARTS[bp.part] + "/" + bp.nameSprite + "_idle");
					if (sprites.Length > 0)
					{
						worker.transform.Find("Worker").Find(BodyPartsConstants.BODY_PARTS[bp.part]).GetComponent<Image>().sprite = sprites[0];
						Color color;
						ColorUtility.TryParseHtmlString(bp.color, out color);
						worker.transform.Find("Worker").Find(BodyPartsConstants.BODY_PARTS[bp.part]).GetComponent<Image>().color = color;
					}
				}
			}
		}
		else
		{
			throw new Exception("Content not found in workers");
		}
	}

	public void FireWorker(int idWorker)
	{
		projectDown.Play();
		core.FireWorker(idWorker);
		OpenCurrentWorkers();
	}

	public void RefuseWorker(int idWorker)
	{
		projectDown.Play();
		core.RefuseWorker(idWorker);
		OpenProposalWorkers();
	}

	public void HireWorker(int idWorker)
	{
		assignation.Play();
		core.HireWorker(idWorker);
		OpenProposalWorkers();
	}

	// Auto Save


	public void OpenAutoSavingPanel()
	{

		Helper.FindDeepChild(bottomMenu.transform, "AutoSavingText").gameObject.SetActive(true);
		StartCoroutine("CloseAutoSavingPanelCo");

	}

	private void CloseAutoSavingPanel()
	{

		Helper.FindDeepChild(bottomMenu.transform, "AutoSavingText").gameObject.SetActive(false);

	}

	private IEnumerator CloseAutoSavingPanelCo()
	{
		yield return new WaitForSeconds(timeDisplayAutoSave);
		CloseAutoSavingPanel();
	}

	public void ReturnMenu()
	{
		Debug.Log("Canceling idProject: ");
		click.Play();

		genericAcceptRefusePanel.SetActive(true);

		RemoveTransparencyCurrentButton();
		currentButton = bottomMenu.transform.Find("ButtonsRight").transform.Find("ExitButton").gameObject;
		RawImage currentButtonImage = currentButton.GetComponent<RawImage>();
		Color tempColor = currentButtonImage.color;
		tempColor.a = 0.6f;
		currentButton.GetComponent<RawImage>().color = tempColor;

		if(currentPanel != null)
			currentPanel.SetActive(false);
		currentPanel = genericAcceptRefusePanel;
		Helper.FindDeepChild(genericAcceptRefusePanel.transform, "Title").GetComponent<Text>().text = "Salir";
		Helper.FindDeepChild(genericAcceptRefusePanel.transform, "Description").GetComponent<Text>().text = "Estás seguro que deseas salir? \n\r" +
			"Se perderán los datos del último mes \r\n";
		Helper.FindDeepChild(genericAcceptRefusePanel.transform, "CancelButton").GetComponent<Button>().onClick.RemoveAllListeners();
		Helper.FindDeepChild(genericAcceptRefusePanel.transform, "CancelButton").GetComponent<Button>().onClick.AddListener(() => { ClosePopUp(); });
		Helper.FindDeepChild(genericAcceptRefusePanel.transform, "ConfirmButton").GetComponent<Button>().onClick.RemoveAllListeners();
		Helper.FindDeepChild(genericAcceptRefusePanel.transform, "ConfirmButton").GetComponent<Button>().onClick.AddListener(() => { ReturnMenuDefinitive(); });
	}

	public void ReturnMenuDefinitive()
	{
		LevelManager.LoadMenu();
	}
}
