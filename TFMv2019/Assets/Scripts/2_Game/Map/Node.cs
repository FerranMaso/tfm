﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Node
{
	public class Connection
	{
		public Node destination;
		public int cost;
		public bool used;
	}

	public Vector3Int pos;
	public float StraightLineDistanceToEnd;
	public float maxCost;
	public List<Connection> destinations;

	public Node(Vector3Int p, List<Connection> l)
	{
		pos = p;
		StraightLineDistanceToEnd = -1;
		destinations = l;
		maxCost = -1;
	}

	public Node(Node n, List<Node> nodeList, Node lastNode)
	{
		nodeList.Add(this);
		pos = n.pos;
		maxCost = -1;
		StraightLineDistanceToEnd = StraightLineDistanceTo(lastNode);
		destinations = new List<Connection>();
		foreach (Connection c in n.destinations)
		{
			Node m = SearchNode(c.destination.pos, nodeList);
			if (m == null)
			{
				Connection new_connection = new Connection
				{

					destination = new Node(c.destination, nodeList, lastNode),
					cost = c.cost
				};
				destinations.Add(new_connection);
			}
			else
			{
				Connection new_connection = new Connection
				{

					destination = m,
					cost = c.cost
				};
				destinations.Add(new_connection);
			}
		}

	}

	public float StraightLineDistanceTo(Node end)
	{
		Debug.Log(end.pos);
		return (float)Math.Sqrt(Math.Pow(((double)pos.x - (double)end.pos.x), 2d) +
						 Math.Pow(((double)pos.y - (double)end.pos.y), 2d));
	}

	public List<Connection> GetLessDestinationNotVisited(int acum_cost)
	{
		List<Connection> n = new List<Connection>();
		foreach (Connection c in destinations)
		{
			if (c.destination.maxCost == -1)
			{
				n.Add(c);
				c.destination.maxCost = acum_cost;
				c.used = false;
			}
		}
		n.Sort((n1, n2) => n1.destination.StraightLineDistanceToEnd.CompareTo(n2.destination.StraightLineDistanceToEnd));
		return n;
	}


	public static Node SearchNode(Vector3Int v, List<Node> nodes)
	{
		try
		{
			foreach (Node n in nodes)
			{
				if (n.pos == v) return n;
			}
			return null;
		}
		catch (Exception ex)
		{
			Debug.LogError("Error searching node");
			return null;
		}
	}

}

