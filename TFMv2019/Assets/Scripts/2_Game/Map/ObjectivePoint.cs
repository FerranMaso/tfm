﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectivePoint : MonoBehaviour
{
	public enum ObjectiveType
	{
		chair,
		point,
		home
	}
	public Directions.Direction direction;
	public Transform modifiedPosition;
	[HideInInspector] public Vector3 position;
	public ObjectiveType objectiveType;
	public Vector2 characterDesp;
	public bool isOcupied = false;
	public GameObject objectAssigned;
    // Start is called before the first frame update
    void Start()
    {
		if (modifiedPosition != null)
		{
			position = modifiedPosition.position;
			//Debug.Log(position);
		}
		else position = GetComponent<Transform>().position;
		
    }


}
