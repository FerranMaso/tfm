﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;


public class PlayerController : MonoBehaviour
{
	public int idPlayer;

	Actions.PlayerActions currentAction;

	public Character character;
	PlayerMovement playerMovement;

	Core core;
	Project projectAssigned;

	public float waitTime = 8f;

	public Animator projectAnimator;

	private float currentTime;
	private float currentTimeIdle;

	private bool isFired = false;
	
    // Start is called before the first frame update
    void Start()
    {
		currentTimeIdle = 0f;
		currentTime = 0f;
		playerMovement = GetComponent<PlayerMovement>();
		core = GameObject.Find("CoreScripts").GetComponent<Core>();
		isFired = false;

	}

    // Update is called once per frame
    void Update()
    {
		if(currentAction == Actions.PlayerActions.idle)
		{
			currentTimeIdle += Time.deltaTime;
			if(currentTimeIdle > waitTime)
			{
				currentTimeIdle -= waitTime;
				playerMovement.GoToWorkplace(projectAssigned != null);
			}
		}
		if (currentAction == Actions.PlayerActions.working)
		{

			if (projectAssigned == null) playerMovement.GoToWorkplace(false);
			else
			{
				currentTime += Time.deltaTime;
				if (currentTime >= 0.5f)
				{
					currentTime = currentTime - 0.5f;

					//the function is a aproximation 
					//sum of development, typing speed and inteligence
					//in 1 day we get by default the half of current seconds in a medium status for 10 seconds 5
					//in a project that have 60 days of duration we must get 1.7 every day
					//in fact we must have a 60%

					//we have that 10 days is equivalent on a variation of 50%
					// 20 days is a variation of 0
					// 60 days is a variation of -33
					// 120 days is a variation of -41.7
					// max days 1000 is a variation of -49%

					//todo: make the variation variable in function of the seconds of every day instead of days

					float variation = 0f;
					float days = (float)projectAssigned.estimatedTimeDays;
					if (days > 0 && days < 20f)
					{
						variation = (days - 10f) * 50f / (20f - 10f);
					}
					else if (days > 20f && days <= 60f)
					{
						variation = (days - 20f) * (-33f) / (60f - 20f);
					}
					else if (days > 60f && days <= 120f)
					{
						variation = (days - 60f) * (-41.7f - (-33f)) / (120f - 60f) - 33f;
					}
					else if (days > 120f && days <= 1000f)
					{
						variation = (days - 120f) * (-49f - (-41.7f)) / (1000f - 120f) - 41.7f;
					}
					else if (days <= 0 || days > 1000f)
					{
						throw new Exception("Invalid days of the project");
					}
					Debug.Log("Variation: " + variation);

					float globalLevel = ((float)GetSkill(Skills.DEVELOPMENT).level + (float)GetSkill(Skills.TYPING_SPEED).level + (float)GetSkill(Skills.INTELIGENCE).level) / 3f;

					if (Random.Range(0, 100 + 1) <= globalLevel + variation)
					{
						//we have a result let's check if it is good or bad

						int randomLanguage = Random.Range(0, 100 + 1);
						ProjectLanguage projectLanguage = null;
						foreach (ProjectLanguage pl in projectAssigned.languages)
						{
							randomLanguage -= pl.percentageUse;
							if (randomLanguage <= 0)
							{
								projectLanguage = pl;
								break;
							}
						}
						if (projectLanguage == null)
						{
							throw new Exception("The project " + projectAssigned.idProject + "- " + projectAssigned.title + " languages usages don't sum 100%");
						}
						int randomLanguageExperience = Random.Range(0, 100 + 1);
						float languageKnowledge = (float)GetLanguage(projectLanguage.languageName).knowledge;

						float range1 = 0;
						float range2 = 0;
						float range3 = 0;
						float range4 = 0;
						float range5 = 0;

						//2 types of scalability depending of the knowledge 100 - 50, 50 - 0
						// 100 ranges: .8 .1 .05 .03 .02
						// 50 ranges: .2 .2 .2 .2 .2
						// 10 ranges: .02 .03 .05 .1. 8
						// 0 ranges: .0 .0 .0 .0 .10
						//test with 75
						if (languageKnowledge > 50f)
						{
							range1 = (languageKnowledge - 50f) / (100f - 50f) * (.8f + .2f) * 100f; //5
							range2 = (languageKnowledge - 50f) / (100f - 50f) * (.1f + .2f) * 100f; //1.5
							range3 = (languageKnowledge - 50f) / (100f - 50f) * (.05f + .2f) * 100f; //1.25
							range4 = (languageKnowledge - 50f) / (100f - 50f) * (.03f + .2f) * 100f; //1.15
							range5 = (languageKnowledge - 50f) / (100f - 50f) * (.02f + .2f) * 100f; //1.1
						}
						else if (languageKnowledge > 10f && languageKnowledge <= 50f)
						{
							range1 = (languageKnowledge - 10f) / (50f - 10f) * (.2f + .02f) * 100f;
							range2 = (languageKnowledge - 10f) / (50f - 10f) * (.2f + .02f) * 100f;
							range3 = (languageKnowledge - 10f) / (50f - 10f) * (.2f + .02f) * 100f;
							range4 = (languageKnowledge - 10f) / (50f - 10f) * (.2f + .02f) * 100f;
							range5 = (languageKnowledge - 10f) / (50f - 10f) * (.2f + .02f) * 100f;
						}
						else if (languageKnowledge > 0f)
						{
							range1 = languageKnowledge / (10f) * .02f * 100f;
							range2 = languageKnowledge / (10f) * .03f * 100f;
							range3 = languageKnowledge / (10f) * .05f * 100f;
							range4 = languageKnowledge / (10f) * .1f * 100f;
							range5 = languageKnowledge / (10f) * (.8f + .2f) * 100f;
						}

						if (randomLanguageExperience <= range1)
						{
							projectAnimator.SetTrigger("ArrowUpDouble");
							core.ProjectAdvance(projectAssigned.idProject, 100);

						}
						else if (randomLanguageExperience <= range1 + range2)
						{
							projectAnimator.SetTrigger("ArrowUp");
							core.ProjectAdvance(projectAssigned.idProject, 75);

						}
						else if (randomLanguageExperience <= range1 + range2 + range3)
						{
							projectAnimator.SetTrigger("ArrowMiddle");
							core.ProjectAdvance(projectAssigned.idProject, 50);

						}
						else if (randomLanguageExperience <= range1 + range2 + range3 + range4)
						{
							projectAnimator.SetTrigger("ArrowDown");
							core.ProjectAdvance(projectAssigned.idProject, 25);

						}
						else if (randomLanguageExperience <= range1 + range2 + range3 + range4 + range5)
						{
							projectAnimator.SetTrigger("ArrowDownDouble");
							core.ProjectAdvance(projectAssigned.idProject, 0);

						}

						// let's check if skill and language is up
						//skills: typing, development, inteligence
						//language that is selected
						float randomLearning = Random.Range(0, 1000 + 1);
						float skill = ((float)GetSkill(Skills.LEARNING).level * 2f + (float)GetSkill(Skills.INTELIGENCE).level) / 3f * 0.1f;
						if (randomLearning < skill)
						{
							IncreaseLanguage(projectLanguage.languageName);
							UpdateCharacterInCore();


						}

						randomLearning = Random.Range(0, 1000 + 1);
						skill = ((float)GetSkill(Skills.LEARNING).level) * 0.1f;
						if (randomLearning < skill)
						{
							IncreaseSkill(Skills.TYPING_SPEED);
						}

						randomLearning = Random.Range(0, 1000 + 1);
						skill = ((float)GetSkill(Skills.LEARNING).level) * 0.1f;
						if (randomLearning < skill)
						{
							IncreaseSkill(Skills.DEVELOPMENT);
						}

						randomLearning = Random.Range(0, 1500 + 1);
						skill = ((float)GetSkill(Skills.LEARNING).level) * 0.1f;
						if (randomLearning < skill)
						{
							IncreaseSkill(Skills.INTELIGENCE);
						}
						UpdateCharacterInCore();
					}
				}
			}
		}
    }

	public void UpdateCharacterInCore()
	{
		int position = core.ExistWorkerInCurrent(character.idWorker);
		if (position != -1) core.enterprise.workers[position] = character;
	}

	public void SetAction(Actions.PlayerActions action)
	{
		currentAction = action;
		if(currentAction == Actions.PlayerActions.atHome && isFired)
		{
			Destroy(gameObject);
		}
	}

	internal void GoHome()
	{
		playerMovement.GoHome();
	}

	internal void GoToWorkplace()
	{
		playerMovement.GoToWorkplace(projectAssigned != null);
	}

	internal void Fire()
	{
		isFired = true;
		playerMovement.Fire();
	}

	public CharacterSkill GetSkill(int idSkill)
	{
		int i = 0;
		while(i < character.skills.Length)
		{
			if(character.skills[i].idSkill == idSkill)
			{
				return character.skills[i];
			}
			i++;
		}
		return AddSkill(idSkill);

	}

	public CharacterSkill AddSkill(int idSkill)
	{
		Helper.Resize(ref character.skills, character.skills.Length + 1);
		character.skills[character.skills.Length-1] = new CharacterSkill()
		{
			idSkill = idSkill,
			nameSkill = Skills.skillsName[idSkill - 1],
			level = Skills.initialSkillLevel,
			maxLevel = 100
		};
		return character.skills[character.skills.Length - 1];
	}

	public CharacterLanguage GetLanguage(string languageName)
	{
		int i = 0;
		while (i < character.languages.Length)
		{
			if (character.languages[i].languageName == languageName)
			{
				return character.languages[i];
			}
			i++;
		}
		return AddLanguage(languageName);

	}

	public void IncreaseLanguage(string languageName)
	{
		int i = 0;
		while (i < character.languages.Length)
		{
			if (character.languages[i].languageName == languageName)
			{
				character.languages[i].knowledge++;
				break;
			}
			i++;
		}
		//todo: advertise increase language
	}

	public void IncreaseSkill(int idSkill)
	{
		int i = 0;
		while (i < character.skills.Length)
		{
			if (character.skills[i].idSkill == idSkill)
			{
				character.skills[i].level++;
				if (character.skills[i].level > character.skills[i].maxLevel)
					character.skills[i].level = character.skills[i].maxLevel;
				break;
			}
			i++;
		}
		//todo: advertise increase skill
	}

	public CharacterLanguage AddLanguage(string languageName)
	{
		Helper.Resize(ref character.languages, character.languages.Length + 1);
		character.languages[character.languages.Length - 1] = new CharacterLanguage()
		{
			languageName = languageName,
			knowledge = Skills.initialSkillLevel
		};
		return character.languages[character.languages.Length - 1];
	}

	internal void AssignProject(Project project)
	{
		projectAssigned = project;
	}
}
