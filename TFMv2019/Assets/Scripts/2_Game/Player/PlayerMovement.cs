﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

#if UNITY_EDITOR
	using UnityEditor;
#endif

public class PlayerMovement : MonoBehaviour
{

	public float smoothMoveSpeed = 2f;

	List<Node> pathNodes;
	public Transform player;
	public TilemapNavigation tmn;

	public AudioSource steps;
	public AudioSource typing;

	//public Animator AnimatorPlayer;

	public Animator[] AnimatorParts;

	private AnimatorOverrideController AnimatorOverrideHair;
	private AnimatorOverrideController AnimatorOverrideHead;
	private AnimatorOverrideController AnimatorOverrideBody;
	private AnimatorOverrideController AnimatorOverrideShirt;
	private AnimatorOverrideController AnimatorOverrideLegs;
	private AnimatorOverrideController AnimatorOverrideTrousers;
	private AnimatorOverrideController AnimatorOverrideFeet;

	public ObjectivePoint workingPosition;
	public ObjectivePoint homePosition;
	public ObjectivePoint actualPosition;
	public ObjectivePoint nextPosition;

	public float xDesp;
	public float yDesp;

	bool running = false;

	[HideInInspector]
	public Directions.Direction walkDirec = Directions.Direction.idle;
	private PlayerController playerController;

	// Use this for initialization
	void Start()
	{
		actualPosition = null;
		pathNodes = new List<Node>();
		playerController = GetComponent<PlayerController>();
		tmn = GameObject.Find("Grid - Oficina").GetComponent<TilemapNavigation>();
		//SetAnimations();
		steps.volume = LevelManager.sound / 100f;
		typing.volume = LevelManager.sound / 100f;

	}

	// Update is called once per frame
	void Update()
	{
		if (actualPosition != null && running == false)
		{
			if (pathNodes == null) pathNodes = new List<Node>();
			if (pathNodes.Count > 0)
			{
				walkDirec = Directions.Direction.thinkDirection;
				StartCoroutine(SmoothMovePlayer());
				running = true;
				CloseComputer();
				playerController.SetAction(Actions.PlayerActions.walking);
				steps.Play();
				typing.Stop();
			}
			else
			{
				//Debug.Log(new Vector3(player.transform.position.x, player.position.y, 0));
				Vector3 isometricPositionPlayer = Helper.RealToIsometric(new Vector3(player.transform.position.x, player.position.y, 0), new Vector3(0, 0.25f, 0), true) - new Vector3(0, 1, 0);
				//Debug.Log(isometricPositionPlayer);

				//Debug.Log(new Vector3(positions[actualPosition].position.x, positions[actualPosition].position.y, 0));
				Vector3 isometricPositionDestination = Helper.RealToIsometric(new Vector3(actualPosition.position.x, actualPosition.position.y, 0), new Vector3(0, 0.25f, 0), true);
				//Debug.Log(isometricPositionDestination);
				pathNodes = tmn.GetShortestPathAstar(new Vector3Int((int)isometricPositionPlayer.x, (int)isometricPositionPlayer.y, 0),
														new Vector3Int((int)isometricPositionDestination.x, (int)isometricPositionDestination.y, 0));
				/*pathNodes = tmn.GetShortestPathAstar(new Vector3Int((int)(player.transform.position.x - tmn.tilemapWalkable.tileAnchor.x), 
																	(int)(player.position.y - tmn.tilemapWalkable.tileAnchor.y), 0),
											   		 new Vector3Int((int)positions[actualPosition].position.x, 
													 				(int)positions[actualPosition].position.y, 0));*/

				Node prevNode = null;
				foreach (Node n in pathNodes)
				{
					if (prevNode != null)
					{
						Vector3 startPos = Helper.RealToIsometric(new Vector3(n.pos.x, n.pos.y, 0), new Vector3(0, 0.25f, 0), true);
						Vector3 finishPos = Helper.RealToIsometric(new Vector3(prevNode.pos.x, prevNode.pos.y, 0), new Vector3(0, 0.25f, 0), true);

					}
					prevNode = n;
				}
				/*if (actualPosition + 1 >= positions.Length) actualPosition = 0;
				else actualPosition++;*/
			}
		}
	}

	public void GoToWorkplace(bool activeProjects)
	{
		if (activeProjects)
		{
			Debug.Log("Going Work");
			if (actualPosition == null)
				actualPosition = workingPosition;
			else
				nextPosition = workingPosition;
		}
		else
		{
			ObjectivePoint idlePosition = GameObject.Find("Grid - Oficina").GetComponent<TilemapNavigation>().GetRandomIdlePosition();
			if (actualPosition == null)
				actualPosition = idlePosition;
			else
				nextPosition = idlePosition;
		}
	}

	public void GoHome()
	{
		CloseComputer();
		Debug.Log("Going Home");
		if (actualPosition == null)
			actualPosition = homePosition;
		else
			nextPosition = homePosition;
	}

	public void Fire()
	{
		CloseComputer();
		Debug.Log("Fire");
		if (actualPosition == null)
			actualPosition = homePosition;
		else
			nextPosition = homePosition;
	}

	IEnumerator SmoothMovePlayer()
	{
		for (int i = 0, max = pathNodes.Count; i < max; i++)
		{
			bool isOver = false;
			while (!isOver)
			{
				Vector3 isometricPositionPlayer = Helper.RealToIsometric(new Vector3(player.transform.position.x, player.position.y, 0), new Vector3(0, 0.25f, 0), false);

				//Debug.Log("IsometricPlayerPosition:" + isometricPositionPlayer);
				//Debug.Log("Destination point " + i + ": " + pathNodes[i].pos);
				Vector3 offSet = new Vector3(pathNodes[i].pos.x, pathNodes[i].pos.y, 0) - isometricPositionPlayer;
				//Debug.Log("OffSet x: " + offSet.x + ", Offset y: " + offSet.y);
				if (offSet.y > 0.000001f)
				{
					setDirectionAnimation(-1.0f, 1.0f);
					walkDirec = Directions.Direction.BackLeft;
				}
				else if (offSet.y < -0.000001f)
				{
					setDirectionAnimation(1.0f, -1.0f);
					walkDirec = Directions.Direction.FrontRight;
				}
				else if (offSet.x < -0.000001f)
				{
					setDirectionAnimation(-1.0f, -1.0f);
					walkDirec = Directions.Direction.FrontLeft;
				}
				else if (offSet.x > 0.000001f)
				{
					setDirectionAnimation(1.0f, 1.0f);
					walkDirec = Directions.Direction.BackRight;
				}


				Vector3 isometricDesp = offSet.normalized * smoothMoveSpeed * Time.deltaTime;
				//Debug.Log("Isometric desplace: " + isometricDesp);
				player.position = Helper.IsometricToReal(isometricPositionPlayer + isometricDesp, new Vector3(0, 0.25f, 0));
				Vector3 realDestinationPoint = Helper.IsometricToReal(pathNodes[i].pos, new Vector3(0, 0.25f, 0));
				isometricPositionPlayer = Helper.RealToIsometric(new Vector3(player.transform.position.x, player.position.y, 0), new Vector3(0, 0.25f, 0), false);
				//Debug.Log("IsometricPlayerPosition after desp:" + isometricPositionPlayer);
				float destinationDistance = 0.03f;
				if (i == max - 1 && actualPosition.objectiveType == ObjectivePoint.ObjectiveType.chair) destinationDistance = 0.1f;
				if (Vector2.Distance(new Vector2(realDestinationPoint.x, realDestinationPoint.y), new Vector2(player.position.x, player.position.y)) < destinationDistance)
				{
					//Debug.Log("Change destination");
					isOver = true;
					Vector3 isometricPositionFinalDestination = new Vector3(pathNodes[i].pos.x, pathNodes[i].pos.y, 0);
					Vector3 realPositionFinalDestination = Helper.IsometricToReal(isometricPositionFinalDestination, new Vector3(0, 0.25f, 0));
					player.position = realPositionFinalDestination;
				}
				yield return new WaitForFixedUpdate();
			}
		}
		steps.Stop();
		if (actualPosition.objectiveType == ObjectivePoint.ObjectiveType.chair)
		{
			player.position = player.position + new Vector3(actualPosition.characterDesp.x, actualPosition.characterDesp.y);
			typing.Play();
			playerController.SetAction(Actions.PlayerActions.working);
			OpenComputer();
			TypingPlayer(actualPosition.direction);
		}
		else if (actualPosition.objectiveType == ObjectivePoint.ObjectiveType.point)
		{
			playerController.SetAction(Actions.PlayerActions.idle);
			StopPlayer(actualPosition.direction);
		}
		else if (actualPosition.objectiveType == ObjectivePoint.ObjectiveType.home)
		{
			playerController.SetAction(Actions.PlayerActions.atHome);
			StopPlayer(actualPosition.direction);
		}
		if (nextPosition == null)
		{
			actualPosition = null;
		}
		else
		{
			actualPosition = nextPosition;
			nextPosition = null;
		}
		running = false;
		pathNodes.Clear();

	}

	public void OpenComputer()
	{
		if (workingPosition != null && workingPosition.objectAssigned != null && workingPosition.objectAssigned.GetComponentInChildren<Animator>() != null)
			workingPosition.objectAssigned.GetComponentInChildren<Animator>().SetBool("Working", true);
	}

	public void CloseComputer()
	{
		if(workingPosition != null && workingPosition.objectAssigned != null && workingPosition.objectAssigned.GetComponentInChildren<Animator>() != null)
			workingPosition.objectAssigned.GetComponentInChildren<Animator>().SetBool("Working", false);
	}


	private void TypingPlayer(Directions.Direction d)
	{
		Debug.Log("Typing player");

		switch (d)
		{
			case Directions.Direction.BackLeft:
				setDirectionAnimation(-0.5f, 0.5f);
				break;
			case Directions.Direction.BackRight:
				setDirectionAnimation(0.5f, 0.5f);
				break;
			case Directions.Direction.FrontLeft:
				setDirectionAnimation(-0.5f, -0.5f);
				break;
			case Directions.Direction.FrontRight:
				setDirectionAnimation(0.5f, -0.5f);
				break;
		}
		walkDirec = d;
	}

	private void StopPlayer(Directions.Direction d)
	{
		Debug.Log("Stopping player");

		switch (d)
		{
			case Directions.Direction.BackLeft:
				setDirectionAnimation(-0.1f, 0.1f);
				break;
			case Directions.Direction.BackRight:
				setDirectionAnimation(0.1f, 0.1f);
				break;
			case Directions.Direction.FrontLeft:
				setDirectionAnimation(-0.1f, -0.1f);
				break;
			case Directions.Direction.FrontRight:
				setDirectionAnimation(0.1f, -0.1f);
				break;
		}
		walkDirec = d;
	}

	private void setDirectionAnimation(float xDesp, float yDesp)
	{
		foreach (Animator anim in AnimatorParts)
		{
			anim.SetFloat("XDesp", xDesp);
			anim.SetFloat("YDesp", yDesp);
			//anim.Play(0, -1, 0);
		}
	}

	public void SetAnimations()
	{
		if(playerController == null) playerController = GetComponent<PlayerController>();
		AssignAnimationToController(BodyPartsConstants.HEAD, playerController.character.bodyParts[BodyPartsConstants.HEAD].nameSprite);
		AssignAnimationToController(BodyPartsConstants.BODY, playerController.character.bodyParts[BodyPartsConstants.BODY].nameSprite);
		AssignAnimationToController(BodyPartsConstants.HAIR, playerController.character.bodyParts[BodyPartsConstants.HAIR].nameSprite);
		AssignAnimationToController(BodyPartsConstants.SHIRT, playerController.character.bodyParts[BodyPartsConstants.SHIRT].nameSprite);
		//AssignAnimationToController(BodyPartsConstants.TROUSERS, playerController.character.bodyParts[BodyPartsConstants.TROUSERS].nameSprite);
		AssignAnimationToController(BodyPartsConstants.LEGS, playerController.character.bodyParts[BodyPartsConstants.LEGS].nameSprite);
		AssignAnimationToController(BodyPartsConstants.FOOT, playerController.character.bodyParts[BodyPartsConstants.FOOT].nameSprite);

		AssignColor(BodyPartsConstants.HEAD, playerController.character.bodyParts[BodyPartsConstants.HEAD].color);
		AssignColor(BodyPartsConstants.BODY, playerController.character.bodyParts[BodyPartsConstants.BODY].color);
		AssignColor(BodyPartsConstants.HAIR, playerController.character.bodyParts[BodyPartsConstants.HAIR].color);
		AssignColor(BodyPartsConstants.SHIRT, playerController.character.bodyParts[BodyPartsConstants.SHIRT].color);
		//AssignColor(BodyPartsConstants.TROUSERS, playerController.character.bodyParts[BodyPartsConstants.TROUSERS].color);
		AssignColor(BodyPartsConstants.LEGS, playerController.character.bodyParts[BodyPartsConstants.TROUSERS].color);
		AssignColor(BodyPartsConstants.FOOT, playerController.character.bodyParts[BodyPartsConstants.FOOT].color);

	}
	private void AssignAnimationToController(int position, string image)
	{
		if (AnimatorParts.Length > position)
		{
			AnimatorOverrideController animatorOverrideController = new AnimatorOverrideController(AnimatorParts[position].runtimeAnimatorController);
			animatorOverrideController.name = position + "OverrideController";
			animatorOverrideController["WalkFrontLeft"] = GetAnimationDirection("FrontLeft", BodyPartsConstants.BODY_RELATION_SINGLE_PARTS[position], image, "walk");
			animatorOverrideController["WalkFrontRight"] = GetAnimationDirection("FrontRight", BodyPartsConstants.BODY_RELATION_SINGLE_PARTS[position], image, "walk");
			animatorOverrideController["WalkBackLeft"] = GetAnimationDirection("BackLeft", BodyPartsConstants.BODY_RELATION_SINGLE_PARTS[position], image, "walk");
			animatorOverrideController["WalkBackRight"] = GetAnimationDirection("BackRight", BodyPartsConstants.BODY_RELATION_SINGLE_PARTS[position], image, "walk");

			animatorOverrideController["IdleFrontLeft"] = GetAnimationDirection("FrontLeft", BodyPartsConstants.BODY_RELATION_SINGLE_PARTS[position], image + "_idle", "idle");
			animatorOverrideController["IdleFrontRight"] = GetAnimationDirection("FrontRight", BodyPartsConstants.BODY_RELATION_SINGLE_PARTS[position], image + "_idle", "idle");
			animatorOverrideController["IdleBackLeft"] = GetAnimationDirection("BackLeft", BodyPartsConstants.BODY_RELATION_SINGLE_PARTS[position], image + "_idle", "idle");
			animatorOverrideController["IdleBackRight"] = GetAnimationDirection("BackRight", BodyPartsConstants.BODY_RELATION_SINGLE_PARTS[position], image + "_idle", "idle");

			animatorOverrideController["TypingFrontLeft"] = GetAnimationDirection("FrontLeft", BodyPartsConstants.BODY_RELATION_SINGLE_PARTS[position], image + "_typing", "typing");
			animatorOverrideController["TypingFrontRight"] = GetAnimationDirection("FrontRight", BodyPartsConstants.BODY_RELATION_SINGLE_PARTS[position], image + "_typing", "typing");
			animatorOverrideController["TypingBackLeft"] = GetAnimationDirection("BackLeft", BodyPartsConstants.BODY_RELATION_SINGLE_PARTS[position], image + "_typing", "typing");
			animatorOverrideController["TypingBackRight"] = GetAnimationDirection("BackRight", BodyPartsConstants.BODY_RELATION_SINGLE_PARTS[position], image + "_typing", "typing");
			AnimatorParts[position].runtimeAnimatorController = animatorOverrideController;
		}
	}

	private void AssignColor(int position, string color)
	{
		if (ColorUtility.TryParseHtmlString(color, out Color newColor))
		{
			AnimatorParts[position].GetComponent<SpriteRenderer>().color = newColor;
		}
	}


	private AnimationClip GetAnimationDirection(string direction, string bodyPosition, string image, string type)
	{
		/*GameObject tempObj = Resources.Load("Animations/" + direction + "/" + bodyPosition + "/" + image, typeof(GameObject)) as GameObject;
		if (tempObj == null)
		{
			Debug.LogError("Animation NOT found: " + "Animations/" + direction + "/" + bodyPosition + "/" + image + ".anim");
			return null;
		}
		else
		{
			Animation animation = tempObj.GetComponent<Animation>();
			AnimationClip animanClip = animation.clip;
			return animanClip;
			
		}*/
		AnimationClip anim = Resources.Load<AnimationClip>("Animations/" + direction + "/" + bodyPosition + "/" + image);
		if (anim == null) Debug.LogError("Error loading animation: " + "Animations/" + direction + "/" + bodyPosition + "/" + image);
		return anim;
	}

	#if UNITY_EDITOR
		public static void CreateAnimations()
		{
			using (StreamReader sw = File.OpenText(Application.streamingAssetsPath + "/CharacterSelection.json"))
			{
				CharacterSelection cs = JsonUtility.FromJson<CharacterSelection>(sw.ReadToEnd());

				foreach (string actions in Actions.ActionsAnimations)
				{
					foreach (string direction in Directions.DirectionsName)
					{
						foreach (var item in cs.characterSelection)
						{
							switch (item.namePart)
							{
								case "Hair":
									foreach (var item2 in item.nameSprite)
									{
										string finalName = "";
										if (actions != "walk") finalName = "_" + actions;
										CreateAnimationDirection(direction, item.namePart, item2 + finalName, actions);
									}
									break;
								case "Shirt":
									foreach (var item2 in item.nameSprite)
									{
										string finalName = "";
										if (actions != "walk") finalName = "_" + actions;
										CreateAnimationDirection(direction, "Body", item2 + finalName, actions);
									}
									break;
								case "Trousers":
									foreach (var item2 in item.nameSprite)
									{
										string finalName = "";
										if (actions != "walk") finalName = "_" + actions;
										CreateAnimationDirection(direction, "Legs", item2 + finalName, actions);
									}
									break;
								case "Shoes":
									foreach (var item2 in item.nameSprite)
									{
										string finalName = "";
										if (actions != "walk") finalName = "_" + actions;
										CreateAnimationDirection(direction, "Foot", item2 + finalName, actions);
									}
									break;
								default:
									break;
							}
						}
						foreach (string singlePart in BodyPartsConstants.SINGLE_PARTS)
						{
							string finalName = "";
							if (actions != "walk") finalName = "_" + actions;
							CreateAnimationDirection(direction, singlePart, singlePart + finalName, actions);
						}
					}
				}
			}
		}

		private static void CreateAnimationDirection(string direction, string bodyPosition, string image, string type)
		{
			Sprite[] sprites = Resources.LoadAll<Sprite>("Sprites/Characters/" + direction + "/" + bodyPosition + "/" + image); // load all sprites in "assets/Resources/sprite" folder
			if (sprites.Length > 0) {
				AnimationClip animClip = new AnimationClip();
				animClip.name = type + direction + bodyPosition;

				//if (repeat) animClip.wrapMode = WrapMode.Loop;

				var settings = AnimationUtility.GetAnimationClipSettings(animClip);
				settings.loopTime = true;
				AnimationUtility.SetAnimationClipSettings(animClip, settings);
				if (type == "typing" || type == "walk")
					animClip.frameRate = 12;   // FPS
				else
					animClip.frameRate = 1;
				EditorCurveBinding spriteBinding = new EditorCurveBinding();
				spriteBinding.type = typeof(SpriteRenderer);
				spriteBinding.path = "";
				spriteBinding.propertyName = "m_Sprite";
				ObjectReferenceKeyframe[] spriteKeyFrames = new ObjectReferenceKeyframe[sprites.Length];
				for (int i = 0; i < (sprites.Length); i++)
				{
					//Debug.Log("direction" + direction + " " + (float)i/10f);
					spriteKeyFrames[i] = new ObjectReferenceKeyframe();
					//AQUÍ HAURIA DE SER DIVIT ENTRE 100 PERÒ NOMÉS FUNCIONA AMB 10... NO TÉ MASSA SENTIT
					//Els segons haurien de ser 0.01 però la transformació final la fa en 0.1 -> 0:01
					spriteKeyFrames[i].time = (float)i / 10f;
					spriteKeyFrames[i].value = sprites[i];
				}
				UnityEditor.AnimationUtility.SetObjectReferenceCurve(animClip, spriteBinding, spriteKeyFrames);
				Directory.CreateDirectory("Assets/Resources/Animations/" + direction + "/" + bodyPosition);
				AssetDatabase.CreateAsset(animClip, "Assets/Resources/Animations/" + direction + "/" + bodyPosition + "/" + image + ".anim");
			}
				//return animClip;
		}
	#endif
}