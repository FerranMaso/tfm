﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using Random = UnityEngine.Random;
using System.IO;
using System.Text.RegularExpressions;


public class Core : MonoBehaviour
{
	public const int defaultGameId = 0;
	//Time properties
	

	//TODO: make public
	private int dayTimeRelation = 10;
	public int defaultDay = 31;
	public int defaultMonth = 5;
	public int defaultYear = 2019;
	private float time;
	private DateTime actualDate;
	private DateTime startDay;

	public GameObject playerPrefab;

	[Header("Project")]
	public int maxProjectProposal = 5;

	public int maxWorkerProposal = 5;

	[HideInInspector]
	public Enterprise enterprise;
	public ProjectsDefault projectsDefault;
	public WorkersDefault workersDefault;


	public ObjectivePoint[] workingPositions;
	public ObjectivePoint[] homePositions;

	private Transform playersContainer; 

	private UIController uiController;

	private float incomeMoney;

	private int lastIdWorker = 1;

	private List<int> currentWorkingPosition;

	//workers List
	private List<string> skinColors;
	private List<Sprite> hairList;
	private List<string> hairColors;
	private List<Sprite> shirtList;
	private List<string> shirtColors;
	private List<Sprite> trousersList;
	private List<string> trousersColors;
	private List<Sprite> shoesList;
	private List<string> shoesColors;
	private List<string> characterNames;
	private List<string> characterSurnames;

	public AudioSource gameMusic;

	private void Awake()
	{
		#if UNITY_EDITOR
			PlayerMovement.CreateAnimations();
		#endif
		enterprise = LevelManager.enterprise;

		if (enterprise == null)
		{
			DefaultGameStart();
			if (enterprise == null)
				throw new Exception("Unable to load game data");
		}
	
		projectsDefault = Helper.ReadProjectsFile();

		startDay = new DateTime(enterprise.foundationYear, enterprise.foundationMonth, enterprise.foundationDay);
		actualDate = new DateTime(enterprise.actualYear, enterprise.actualMonth, enterprise.actualDay);
	}



	// Start is called before the first frame update
	void Start()
    {
		gameMusic.volume = LevelManager.music / 100f; 
		uiController = GetComponent<UIController>();
		uiController.SetDateText(Helper.DateToString(actualDate));
		uiController.SetMoneyText(enterprise.currentMoney.ToString("n2") + "€");

		playersContainer = GameObject.Find("Players").transform;

		foreach(Character c in enterprise.workers)
		{
			AddPlayer(c);

		}

		lastIdWorker = enterprise.lastIdWorker;

		//todo: remove and add in starting new game
		//We add 4 random proposal projects
		AddProposalProject();
		AddProposalProject();
		AddProposalProject();
		AddProposalProject();
		//end todo
		LoadCharacterPartsLists();

		AddCharacterInProposal();
		AddCharacterInProposal();
	}

	public void AddPlayer(Character c)
	{
		GameObject playerInst = Instantiate(playerPrefab, playersContainer);
		playerInst.name = "Worker#" + c.idWorker;
		PlayerController playerController = playerInst.GetComponent<PlayerController>();
		PlayerMovement playerMovement = playerInst.GetComponent<PlayerMovement>();
		playerController.character = c;

		int i = 0;
		while (i < workingPositions.Length && workingPositions[i].isOcupied) {
			i++;
		}
		if (i >= workingPositions.Length) Debug.LogError("All work positions occupied");
		workingPositions[i].isOcupied = true;
		playerMovement.workingPosition = workingPositions[i];

		i = 0;
		while (i < homePositions.Length && homePositions[i].isOcupied) i++;
		if (i >= homePositions.Length) Debug.LogError("All home positions occupied");
		homePositions[i].isOcupied = true;
		playerMovement.homePosition = homePositions[i];
		if(enterprise.projects.doingProjects.Length > 0)
			playerController.AssignProject(enterprise.projects.doingProjects[enterprise.projects.doingProjects.Length - 1]);

		playerMovement.SetAnimations();
	}

	void DefaultGameStart()
	{
		Game g = Helper.ReadGameFile();
		if (g == null || g.enterprises == null)
		{
			Debug.Log("No games found");
		}
		else
		{
			Debug.Log("Games found");
			foreach (Enterprise e in g.enterprises)
			{
				if(e.enterpriseId == defaultGameId)
				{
					enterprise = e;
				}
			}
		}
	}

    // Update is called once per frame
    void Update()
    {
		time += Time.deltaTime;
		if(time >= (float)dayTimeRelation)
		{
			int rand = Random.Range(0, 100 + 1);
			enterprise.secondsPlayed += dayTimeRelation;
			actualDate = actualDate.AddDays(1);
			enterprise.actualDay = actualDate.Day;
			enterprise.actualMonth = actualDate.Month;
			enterprise.actualYear = actualDate.Year;
			time = dayTimeRelation - time;
			uiController.SetDateText(Helper.DateToString(actualDate));

			//todo: que arribin els projectes en funció de la popularitat
			float probabilityProposalProject = (enterprise.popularity + enterprise.reputation) / 2;
			if(rand < 0.5 * probabilityProposalProject) //aprox every 20 days 1 project
			{
				AddProposalProject();
			}

			if (actualDate.DayOfWeek == DayOfWeek.Saturday) 
			{
				foreach (PlayerController player in playersContainer.GetComponentsInChildren<PlayerController>())
				{
					player.GoHome();
				}
			}
			else if(actualDate.DayOfWeek == DayOfWeek.Monday)
			{
				foreach(PlayerController player in playersContainer.GetComponentsInChildren<PlayerController>())
				{
					player.GoToWorkplace();
				}
			}

			float probabilityWorkerPetition = (enterprise.popularity + enterprise.reputation) / 2;
			if (rand < 0.5 * probabilityWorkerPetition) //aprox every 20 days 1 project
			{
				AddCharacterInProposal();
			}

			

			if (actualDate.Month == 1 && actualDate.Day == 1) //year has changed
			{
				EconomicChangesYear();
				uiController.SetMoneyText(enterprise.currentMoney.ToString("n2") + "€");

			}
			else if (actualDate.Day == 1) //month has changed
			{
				EconomicChangesMonth();
				uiController.SetMoneyText(enterprise.currentMoney.ToString("n2") + "€");
				uiController.OpenAutoSavingPanel();
				AutoSave();
			}

		}
    }

	//save

	private void AutoSave()
	{
		enterprise.actualDay = actualDate.Day;
		enterprise.actualMonth = actualDate.Month;
		enterprise.actualYear = actualDate.Year;

		enterprise.savedDay = DateTime.Now.Day;
		enterprise.savedMonth = DateTime.Now.Month;
		enterprise.savedYear = DateTime.Now.Year;
		enterprise.savedHour = DateTime.Now.Hour;
		enterprise.savedMinute = DateTime.Now.Minute;

		Helper.WriteGames(enterprise);

	}

	//time recurrent functions
	void EconomicChangesMonth()
	{
		float salaries = 0;
		float cotization = 0;
		float insurance = 0;

		foreach(Character c in enterprise.workers)
		{
			salaries += c.salary;
			cotization += c.cotization;
			insurance += c.insurance;


		}

		float loansTotal = 0;
		try
		{
			foreach (Loan l in enterprise.loans)
			{
				if (l.remainingMonths > 0)
					loansTotal += l.monthlyPaymentQuantity;
			}
		}
		catch(Exception ex)
		{
			enterprise.loans = new Loan[0];
		}
 
		float generalCosts = enterprise.administrativeCosts + cotization + insurance + loansTotal;
		float salariesCosts = salaries;
		float localCosts = enterprise.office.monthlyCost + enterprise.office.insuranceCost + enterprise.office.basicElectricityCost;

		float balance = incomeMoney - generalCosts - salariesCosts - localCosts;
		string symbol = (balance > 0) ? "+" : "";


		uiController.OpenGenericPanel("Ingresos y gastos final de mes", "Gastos generales: -"+ generalCosts.ToString("n2") + "€\n\r" +
			"Gastos local: -" + localCosts.ToString("n2") + "€\n\r" +
			"Despesa Empleados: -" + salariesCosts.ToString("n2") + "€\r\n\r\n" +
			"Ingresos: +" + incomeMoney.ToString("n2") + "€\r\n\r\n" +
			"Balance: " + symbol + balance.ToString("n2") + "€");

		enterprise.currentMoney = enterprise.currentMoney - generalCosts - salariesCosts - localCosts + incomeMoney;
		incomeMoney = 0;
	}

	void EconomicChangesYear()
	{
		float salaries = 0;
		float cotization = 0;
		float insurance = 0;

		foreach (Character c in enterprise.workers)
		{
			salaries += c.salary;
			cotization += c.cotization;
			insurance += c.insurance;


		}

		float loansTotal = 0;
		try
		{
			foreach (Loan l in enterprise.loans)
			{
				if (l.remainingMonths > 0)
					loansTotal += l.monthlyPaymentQuantity;
			}
		}
		catch (Exception ex)
		{
			enterprise.loans = new Loan[0];
		}

		float generalCosts = enterprise.administrativeCosts + cotization + insurance + loansTotal;
		float salariesCosts = salaries;
		float localCosts = enterprise.office.monthlyCost + enterprise.office.insuranceCost + enterprise.office.basicElectricityCost;
		float yearCosts = enterprise.laboralRisks + enterprise.taxes + enterprise.licenses; 

		uiController.OpenGenericPanel("Ingresos y gastos final de año", "Gastos anuales: -" + yearCosts.ToString("n2") + "€\n\r\n\n" + 
			"Gastos generales: -" + generalCosts.ToString("n2") + "€\n\r" +
			"Gastos local: -" + localCosts.ToString("n2") + "€\n\r" +
			"Despesa Empleados: -" + salariesCosts.ToString("n2") + "€\r\n\r\n" +
			"Ingresos: +" + incomeMoney.ToString("n2") + "€");

		enterprise.currentMoney = enterprise.currentMoney - generalCosts - yearCosts - salariesCosts - localCosts + incomeMoney;
		incomeMoney = 0;

	}

	//project functions

	void AddProposalProject()
	{
		if(enterprise.projects.proposalProjects.Length < maxProjectProposal)
		{
			int num = Random.Range(0, projectsDefault.projects.Length) + 1; //the id starts with 1 and the array with 0
			int positionProjectsDefault = ExistsProjectInProjectDefault(num);
			int position = ExistProjectInProposal(num);
			int position2 = ExistProjectInDoing(num);
			int position3 = ExistProjectInDone(num);
			int iterations = 0;
			while ((position != -1 || position2 != -1 || position3 != -1 || positionProjectsDefault == -1) && iterations < projectsDefault.projects.Length - 1)
			{
				num++;
				if(num >= projectsDefault.projects.Length)
				{
					num = 1;
				}
				positionProjectsDefault = ExistsProjectInProjectDefault(num);
				position = ExistProjectInProposal(num);
				position2 = ExistProjectInDoing(num);
				position3 = ExistProjectInDone(num);
				iterations++;
			}
			if(position == -1 && position2 == -1 && position3 == -1 && positionProjectsDefault != -1)
			{
				Helper.Resize(ref enterprise.projects.proposalProjects, enterprise.projects.proposalProjects.Length + 1);
				Project projectDefault = projectsDefault.projects[positionProjectsDefault];
				enterprise.projects.proposalProjects[enterprise.projects.proposalProjects.Length - 1] = projectDefault;
			}
			else
			{
				//todo: search a most acurate solution
				enterprise.projects.doneProjects = new Project[] { };
			}
		}
	}

	private int ExistsProjectInProjectDefault(int idProject)
	{
		int position = -1;

		for (int i = 0; i < projectsDefault.projects.Length; i++)
		{
			if (projectsDefault.projects[i].idProject == idProject)
			{
				position = i;
			}
		}
		return position;
	}



	private int ExistProjectInProposal(int idProject)
	{
		int position = -1;

		for (int i = 0; i < enterprise.projects.proposalProjects.Length; i++)
		{
			if(enterprise.projects.proposalProjects[i].idProject == idProject)
			{
				position = i;
			}
		}

		return position;
	}

	private int ExistProjectInDoing(int idProject)
	{
		int position = -1;

		for (int i = 0; i < enterprise.projects.doingProjects.Length; i++)
		{
			if (enterprise.projects.doingProjects[i].idProject == idProject)
			{
				position = i;
			}
		}

		return position;
	}

	public Project GetProjectInDoing(int idProject)
	{
		Project proj = null;
		for (int i = 0; i < enterprise.projects.doingProjects.Length; i++)
		{
			if (enterprise.projects.doingProjects[i].idProject == idProject)
			{
				proj = enterprise.projects.doingProjects[i];
			}
		}

		return proj;
	}

	private int ExistProjectInDone(int idProject)
	{
		int position = -1;

		for (int i = 0; i < enterprise.projects.doneProjects.Length; i++)
		{
			if (enterprise.projects.doneProjects[i].idProject == idProject)
			{
				position = i;
			}
		}

		return position;
	}

	public void AcceptProposalProject(int idProject)
	{
		Debug.Log("IdProject, accept proposal: " + idProject);
		int position = ExistProjectInProposal(idProject);
		if(position == -1)
		{
			throw new Exception("Id not found in proposal projects, id: " + idProject);
		}
		Helper.Resize(ref enterprise.projects.doingProjects, enterprise.projects.doingProjects.Length + 1);
		enterprise.projects.doingProjects[enterprise.projects.doingProjects.Length - 1] = enterprise.projects.proposalProjects[position];
		RemoveProposalProject(idProject);
		foreach (PlayerController player in playersContainer.GetComponentsInChildren<PlayerController>())
		{
			player.AssignProject(enterprise.projects.doingProjects[enterprise.projects.doingProjects.Length - 1]);
		}
	}

	public void RefuseProposalProject(int idProject)
	{
		int position = ExistProjectInProposal(idProject);
		if (position == -1)
		{
			throw new Exception("Id not found in proposal projects, id: " + idProject);
		}
		RemoveProposalProject(idProject);
	}

	public void FinishProject(int idProject)
	{
		Debug.Log("IdProject, accept proposal: " + idProject);
		int position = ExistProjectInDoing(idProject);
		if (position == -1)
		{
			throw new Exception("Id not found in proposal projects, id: " + idProject);
		}
		Helper.Resize(ref enterprise.projects.doneProjects, enterprise.projects.doneProjects.Length + 1);
		enterprise.projects.doneProjects[enterprise.projects.doneProjects.Length - 1] = enterprise.projects.doingProjects[position];

		uiController.RemoveCurrentProject(idProject);
		RemoveDoingProject(idProject);
		if (enterprise.projects.doingProjects.Length > 0)
		{
			foreach (PlayerController player in playersContainer.GetComponentsInChildren<PlayerController>())
			{
				player.AssignProject(enterprise.projects.doingProjects[0]);
			}
		}
		else
		{
			foreach (PlayerController player in playersContainer.GetComponentsInChildren<PlayerController>())
			{
				player.AssignProject(null);
			}
		}
	}


	public void CancelProject(int idProject)
	{
		Debug.Log("IdProject, cancel project: " + idProject);
		int position = ExistProjectInDoing(idProject);
		if (position == -1)
		{
			throw new Exception("Id not found in doing projects, id: " + idProject);
		}
		Project proj = enterprise.projects.doingProjects[position];
		enterprise.currentMoney = enterprise.currentMoney - proj.money * (float)Finance.PROJECT_CANCEL_PENALIZATION_MONEY / 100;
		enterprise.reputation = enterprise.reputation - enterprise.reputation * (int)Finance.PROJECT_CANCEL_PENALIZATION_REPUTATION / 100;
		RemoveDoingProject(idProject);
		if (enterprise.projects.doingProjects.Length > 0)
		{
			foreach (PlayerController player in playersContainer.GetComponentsInChildren<PlayerController>())
			{
				player.AssignProject(enterprise.projects.doingProjects[0]);
			}
		}
		else
		{
			foreach (PlayerController player in playersContainer.GetComponentsInChildren<PlayerController>())
			{
				player.AssignProject(null);
			}
		}
	}
 
	private void RemoveProposalProject(int idProject)
	{
		Project[] supportArray = new Project[enterprise.projects.proposalProjects.Length - 1]; int j = 0;
		int position = ExistProjectInProposal(idProject);
		if(position != -1)
		{
			for (int i = 0; i < enterprise.projects.proposalProjects.Length; i++)
			{
				if (idProject != enterprise.projects.proposalProjects[i].idProject)
				{
					supportArray[j] = enterprise.projects.proposalProjects[i];
					j++;
				}
			}
			enterprise.projects.proposalProjects = supportArray;
		}
	}

	public void RemoveDoingProject(int idProject)
	{
		Project[] supportArray = new Project[enterprise.projects.doingProjects.Length - 1]; int j = 0;
		int position = ExistProjectInDoing(idProject);
		for (int i = 0; i < enterprise.projects.doingProjects.Length; i++)
		{
			if (i != position)
			{
				supportArray[j] = enterprise.projects.doingProjects[i];
				j++;
			}
		}
		enterprise.projects.doingProjects = supportArray;
	}

	public Project[] GetProposalProjects()
	{
		return enterprise.projects.proposalProjects;
	}

	public Project[] GetDoingProjects()
	{
		return enterprise.projects.doingProjects;
	}

	public Project[] GetDoneProjects()
	{
		return enterprise.projects.doneProjects;
	}

	public Enterprise GetEnterprise()
	{
		return enterprise;
	}

	public void ProjectAdvance(int idProject, int quality)
	{
		int project = ExistProjectInDoing(idProject);
		if(project != -1)
		{
			enterprise.projects.doingProjects[project].quality = ((enterprise.projects.doingProjects[project].status * enterprise.projects.doingProjects[project].quality) + quality) / (enterprise.projects.doingProjects[project].status + 1);
			Debug.Log(enterprise.projects.doingProjects[project].quality);
			enterprise.projects.doingProjects[project].status++;
			Debug.Log(enterprise.projects.doingProjects[project].status);
			uiController.RefreshCurrentProject(idProject, quality);

			if (enterprise.projects.doingProjects[project].status >= 100)
			{
				float totalQuality = enterprise.projects.doingProjects[project].quality;
				//quality of project is sup on 80% you have tip of 0-20%
				if (enterprise.projects.doingProjects[project].quality >= 80)
				{
					//20% max of the value
					float projectMoney = enterprise.projects.doingProjects[project].money;
					float tip = projectMoney * (20f - (100 - totalQuality));
					float incomeMoneyProject = projectMoney + tip;
					uiController.OpenGenericPanel("Proyecto finalizado", "Proyecto \"" + enterprise.projects.doingProjects[project] + "\" finalizado. \r\n\r\n " +
						"Propina: +" + tip.ToString("n2") + "€\n\r" +
						"Cobro del proyecto a final de mes: " + incomeMoneyProject.ToString("n2"));
					incomeMoney += incomeMoneyProject;
					enterprise.popularity += 1;
					enterprise.reputation += 2;
					if (enterprise.reputation > 100) enterprise.reputation = 100;
					if (enterprise.popularity > 100) enterprise.popularity = 100;

				}
				else //quality of project is inf of 80% penalization in income of 0-20%
				{
					float projectMoney = enterprise.projects.doingProjects[project].money;
					float penalization = (projectMoney * 0.2f) * (80f - (100 - totalQuality)) / 100;
					float incomeMoneyProject = projectMoney + penalization;
					uiController.OpenGenericPanel("Proyecto finalizado", "Proyecto \"" + enterprise.projects.doingProjects[project] + "\" finalizado. \r\n\r\n " +
						"Penalización por calidad: -" + penalization.ToString("n2") + "€\n\r" +
						"Cobro del proyecto a final de mes: " + incomeMoneyProject.ToString("n2"));
					incomeMoney += incomeMoneyProject;
					if(totalQuality < 50)
					{
						enterprise.popularity -= 1;
						enterprise.reputation -= 2;
					}
					else
					{
						enterprise.reputation -= 1;
					}
					if (enterprise.reputation < 0) enterprise.reputation = 0;
				}

				FinishProject(idProject);
			}
		}
	}

	//workers

	private void LoadCharacterPartsLists()
	{
		using (StreamReader sw = File.OpenText(Application.streamingAssetsPath + "/CharacterSelection.json"))
		{
			CharacterSelection cs = JsonUtility.FromJson<CharacterSelection>(sw.ReadToEnd());

			hairList = new List<Sprite>();
			shirtList = new List<Sprite>();
			trousersList = new List<Sprite>();
			shoesList = new List<Sprite>();

			skinColors = new List<string>();
			hairColors = new List<string>();
			shirtColors = new List<string>();
			trousersColors = new List<string>();
			shoesColors = new List<string>();

			// foreach de cada part
			foreach (var item in cs.characterSelection)
			{
				switch (item.namePart)
				{
					case "Skin":
						foreach (var color in item.color)
						{
							skinColors.Add(color);
						}
						break;
					case "Hair":
						foreach (var item2 in item.nameSprite)
						{
							Sprite[] sprites = Resources.LoadAll<Sprite>("Sprites/Characters/FrontRight/Hair/" + item2 + "_idle");
							hairList.Add(sprites[0]);
						}
						foreach (var color in item.color)
						{
							hairColors.Add(color);
						}
						break;
					case "Shirt":
						foreach (var item2 in item.nameSprite)
						{
							Sprite[] sprites = Resources.LoadAll<Sprite>("Sprites/Characters/FrontRight/Body/" + item2 + "_idle");
							shirtList.Add(sprites[0]);
						}
						foreach (var color in item.color)
						{
							shirtColors.Add(color);
						}
						break;
					case "Trousers":
						foreach (var item2 in item.nameSprite)
						{
							Sprite[] sprites = Resources.LoadAll<Sprite>("Sprites/Characters/FrontRight/Legs/" + item2 + "_idle");
							trousersList.Add(sprites[0]);
						}
						foreach (var color in item.color)
						{
							trousersColors.Add(color);
						}
						break;
					case "Shoes":
						foreach (var item2 in item.nameSprite)
						{
							Sprite[] sprites = Resources.LoadAll<Sprite>("Sprites/Characters/FrontRight/Foot/" + item2 + "_idle");
							shoesList.Add(sprites[0]);
						}
						foreach (var color in item.color)
						{
							shoesColors.Add(color);
						}
						break;
					default:
						break;
				}
			}
		}
		using (StreamReader streamR = File.OpenText(Application.streamingAssetsPath + "/CharacterName.json"))
		{
			CharacterName cn = JsonUtility.FromJson<CharacterName>(streamR.ReadToEnd());
			characterNames = new List<string>();
			foreach (string item in cn.name)
			{
				characterNames.Add(item);
			}
			characterSurnames = new List<string>();
			foreach (string item in cn.surname)
			{
				characterSurnames.Add(item);
			}
		}
	}

	public Character[] GetProposalWorkers()
	{
		return enterprise.proposalWorkers;
	}

	public Character[] GetCurrentWorkers()
	{
		return enterprise.workers;
	}

	private int ExistWorkerInProposal(int idWorker)
	{
		int position = -1;

		for (int i = 0; i < enterprise.proposalWorkers.Length; i++)
		{
			if (enterprise.proposalWorkers[i].idWorker == idWorker)
			{
				position = i;
			}
		}

		return position;
	}

	public int ExistWorkerInCurrent(int idWorker)
	{
		int position = -1;

		for (int i = 0; i < enterprise.workers.Length; i++)
		{
			if (enterprise.workers[i].idWorker == idWorker)
			{
				position = i;
			}
		}

		return position;
	}


	public void RefuseWorker(int idWorker)
	{
		Character[] supportArray = new Character[enterprise.proposalWorkers.Length - 1]; int j = 0;
		int position = ExistWorkerInProposal(idWorker);
		if (position != -1)
		{
			for (int i = 0; i < enterprise.proposalWorkers.Length; i++)
			{
				if (idWorker != enterprise.proposalWorkers[i].idWorker)
				{
					supportArray[j] = enterprise.proposalWorkers[i];
					j++;
				}
			}
			enterprise.proposalWorkers = supportArray;
		}
	}

	public void FireWorker(int idWorker)
	{
		Character[] supportArray = new Character[enterprise.workers.Length - 1]; int j = 0;
		int position = ExistWorkerInCurrent(idWorker);
		if (position != -1)
		{
			for (int i = 0; i < enterprise.workers.Length; i++)
			{
				if (idWorker != enterprise.workers[i].idWorker)
				{
					supportArray[j] = enterprise.workers[i];
					j++;
				}
			}
			enterprise.workers = supportArray;
		}
		PlayerController pc = playersContainer.Find("Worker#" + idWorker).GetComponent<PlayerController>();
		PlayerMovement pm = playersContainer.Find("Worker#" + idWorker).GetComponent<PlayerMovement>();
		pm.homePosition.isOcupied = false;
		pm.workingPosition.isOcupied = false;
		pc.Fire();
	}

	public void HireWorker(int idWorker)
	{
		Debug.Log("IdProject, accept proposal: " + idWorker);
		int position = ExistWorkerInProposal(idWorker);
		if (position == -1)
		{
			throw new Exception("Id not found in proposal projects, id: " + idWorker);
		}
		Helper.Resize(ref enterprise.workers, enterprise.workers.Length + 1);
		enterprise.workers[enterprise.workers.Length - 1] = enterprise.proposalWorkers[position];
		AddPlayer(enterprise.workers[enterprise.workers.Length - 1]);
		RefuseWorker(idWorker);
	}

	public void AddCharacterInProposal()
	{
		if (enterprise.proposalWorkers == null) enterprise.proposalWorkers = new Character[] { };
		if (enterprise.proposalWorkers.Length < maxWorkerProposal)
		{

			Helper.Resize(ref enterprise.proposalWorkers, enterprise.proposalWorkers.Length + 1);
			enterprise.proposalWorkers[enterprise.proposalWorkers.Length - 1] = CreateCharacter();

		}
	}

	private Character CreateCharacter()
	{
		int rand_name = Random.Range(0, characterNames.Count - 1);
		int rand_surname = Random.Range(0, characterSurnames.Count - 1);
		int rand_surname2 = Random.Range(0, characterSurnames.Count - 1);
		int rand_skincolor = Random.Range(0, skinColors.Count - 1);
		enterprise.lastIdWorker++;
		int salaryBase = 800;

		int numSkills = Random.Range(0, Skills.skillsName.Length / 2);
		CharacterSkill[] charSkills = new CharacterSkill[numSkills];
		List<int> numsSkills = new List<int>();
		for (int k = 0; k < numSkills; k++)
		{
			int skillNumber = Random.Range(0, Skills.skillsName.Length - 1);
			while(numsSkills.Exists(a => a == skillNumber))
			{
				skillNumber++;
				if (skillNumber >= Skills.skillsName.Length) skillNumber = 0;
			}
			numsSkills.Add(skillNumber);

		}
		int l = 0;
		foreach(int n in numsSkills)
		{
			int percentage = Random.Range(0, 80);
			CharacterSkill cs = new CharacterSkill
			{
				nameSkill = Skills.skillsName[n],
				maxLevel = 100,
				level = percentage
			};
			charSkills[l] = cs;
			salaryBase += percentage * 2;
			l++;
		}


		int numLanguages = Random.Range(0, Language.AllLanguages.Length / 2);
		CharacterLanguage[] charLanguages = new CharacterLanguage[numLanguages];
		List<int> numsLanguages = new List<int>();
		for (int m = 0; m < numLanguages; m++)
		{
			int languageNumber = Random.Range(0, Language.AllLanguages.Length - 1);
			while (numsLanguages.Exists(a => a == languageNumber))
			{
				languageNumber++;
				if (languageNumber >= Language.AllLanguages.Length) languageNumber = 0;
			}
			numsLanguages.Add(languageNumber);

		}
		int h = 0;
		foreach (int n in numsLanguages)
		{
			int percentage = Random.Range(0, 80);
			CharacterLanguage cl = new CharacterLanguage
			{
				languageName = Language.AllLanguages[n],
				knowledge = percentage
			};
			charLanguages[h] = cl;
			salaryBase += percentage * 2;
			h++;
		}

		salaryBase += Random.Range(0, 200);

		string hairName = Regex.Split(hairList[Random.Range(0, hairList.Count)].name, "_idle")[0];
		string shirtName = Regex.Split(shirtList[Random.Range(0, shirtList.Count)].name, "_idle")[0];
		string trousersName = Regex.Split(trousersList[Random.Range(0, trousersList.Count)].name, "_idle")[0];
		string shoesName = Regex.Split(shoesList[Random.Range(0, shoesList.Count)].name, "_idle")[0];

		Character c = new Character
		{
			idWorker = enterprise.lastIdWorker,
			smoothMoveSpeed = 2,
			bodyParts = new BodyParts[] {
					new BodyParts {
						nameSprite = hairName,
						color = hairColors[Random.Range(0, hairColors.Count)],
						part = BodyPartsConstants.HAIR
					},
					new BodyParts {
						nameSprite = "head",
						color = skinColors[rand_skincolor],
						part = BodyPartsConstants.HEAD
					},
					new BodyParts {
						nameSprite = "body",
						color = skinColors[rand_skincolor],
						part = BodyPartsConstants.BODY
					},
					new BodyParts {
						nameSprite = shirtName,
						color = shirtColors[Random.Range(0, shirtColors.Count)],
						part = BodyPartsConstants.SHIRT
					},
					new BodyParts {
						nameSprite = "legs",
						color = skinColors[rand_skincolor],
						part = BodyPartsConstants.LEGS
					},
					new BodyParts {
						nameSprite = trousersName,
						color = trousersColors[Random.Range(0, trousersColors.Count)],
						part = BodyPartsConstants.TROUSERS
					},
					new BodyParts {
						nameSprite = shoesName,
						color = shoesColors[Random.Range(0, shoesColors.Count)],
						part = BodyPartsConstants.FOOT
					}
				},
			skills = charSkills,
			languages = charLanguages,
			nameCharacter = characterNames[rand_name] + " " + characterSurnames[rand_surname] + " " + characterSurnames[rand_surname2],
			motivation = "Tú si que vales!",
			stress = 5,
			hungry = 5,
			energy = 5,
			happiness = 5,
			xPosition = 11.5f,
			yPosition = 1.5f,
			salary = salaryBase,
			insurance = 50
		};

		return c;
	}
}
