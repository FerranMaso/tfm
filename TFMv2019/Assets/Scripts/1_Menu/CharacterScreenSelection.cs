﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using TMPro;
using System.Text.RegularExpressions;

public class CharacterScreenSelection : MonoBehaviour
{

    public TMP_InputField companyName;
    public TMP_InputField playerName;
    public GameObject selection_one;
    public GameObject selection_two;

    private List<string> skinColors;

    public TextMeshProUGUI hairName;
    private List<Sprite> hairList;
    private List<string> hairColors;
    public Image hairViewer;

    public TextMeshProUGUI shirtName;
    private List<Sprite> shirtList;
    private List<string> shirtColors;
    public Image shirtViewer;

    public TextMeshProUGUI trousersName;
    private List<Sprite> trousersList;
    private List<string> trousersColors;
    public Image trousersViewer;

    public TextMeshProUGUI shoesName;
    private List<Sprite> shoesList;
    private List<string> shoesColors;
    public Image shoesViewer;

    public Image head;
    public Image body;
    public Image legs;

    public bool randomClothes = true;
    public bool randomColors = true;

    public GameObject colorPrefab;

    private int indexHair = 0;
    private int indexShirt = 0;
    private int indexTrousers = 0;
    private int indexShoes = 0;
    private Color skinColor;

    private void Start() {
        selection_two.SetActive(true);
        loadChar();
    }

    private void loadChar() {

        using (StreamReader sw = File.OpenText(Application.streamingAssetsPath + "/CharacterSelection.json")) {
            CharacterSelection cs = JsonUtility.FromJson<CharacterSelection>(sw.ReadToEnd());

            hairList = new List<Sprite>();
            shirtList = new List<Sprite>();
            trousersList = new List<Sprite>();
            shoesList = new List<Sprite>();

            skinColors = new List<string>();
            hairColors = new List<string>();
            shirtColors = new List<string>();
            trousersColors = new List<string>();
            shoesColors = new List<string>();

            // foreach de cada part
            foreach (var item in cs.characterSelection) {

                switch (item.namePart) {
                    case "Skin":
                        foreach (var color in item.color) {
                            InstantiateColorPrefab(item, color, "skin color");
                            skinColors.Add(color);
                        }
                        break;
                    case "Hair":
                        foreach (var item2 in item.nameSprite) {
                            Sprite[] sprites = Resources.LoadAll<Sprite>("Sprites/Characters/FrontRight/Hair/" + item2 + "_idle");
                            hairList.Add(sprites[0]);
                        }
                        foreach (var color in item.color) {
                            InstantiateColorPrefab(item, color, "hair color");
                            hairColors.Add(color);
                        }
                        break;
                    case "Shirt":
                        foreach (var item2 in item.nameSprite) {
                            Sprite[] sprites = Resources.LoadAll<Sprite>("Sprites/Characters/FrontRight/Body/" + item2 + "_idle");
                            shirtList.Add(sprites[0]);
                        }
                        foreach (var color in item.color) {
                            InstantiateColorPrefab(item, color, "shirt color");
                            shirtColors.Add(color);
                        }
                        break;
                    case "Trousers":
                        foreach (var item2 in item.nameSprite) {
                            Sprite[] sprites = Resources.LoadAll<Sprite>("Sprites/Characters/FrontRight/Legs/" + item2 + "_idle");
                            trousersList.Add(sprites[0]);
                        }
                        foreach (var color in item.color) {
                            InstantiateColorPrefab(item, color, "trousers color");
                            trousersColors.Add(color);
                        }
                        break;
                    case "Shoes":
                        foreach (var item2 in item.nameSprite) {
                            Sprite[] sprites = Resources.LoadAll<Sprite>("Sprites/Characters/FrontRight/Foot/" + item2 + "_idle");
                            shoesList.Add(sprites[0]);
                        }
                        foreach (var color in item.color) {
                            InstantiateColorPrefab(item, color, "shoes color");
                            shoesColors.Add(color);
                        }
                        break;
                    default:
                        break;
                }
            }

            selection_two.SetActive(false);
            
            setNewHair(0);
            setNewShirt(0);
            setNewTrousers(0);
            setNewShoes(0);

        }

    }

    private void InstantiateColorPrefab(CharacterSelectionParts item, string color, string parent) {
        GameObject colorGO = Instantiate(colorPrefab, transform.position, Quaternion.identity) as GameObject;
        colorGO.name = item.namePart + "_" + color;

        // setting color of the prefab
        if (ColorUtility.TryParseHtmlString(color, out Color newColor)) {
            colorGO.GetComponent<Image>().color = newColor;
        }

        // adding event on color click
        colorGO.GetComponent<Button>().onClick.AddListener(() => ColorItem(item.namePart.ToLower(), ColorUtility.ToHtmlStringRGB(newColor)));

         colorGO.transform.SetParent(GameObject.Find(parent).transform);
    }

    public void setClothesNames() {
        hairName.SetText(" Hair #" + (indexHair + 1));
        shirtName.SetText(" Shirt #" + (indexShirt + 1));
        trousersName.SetText(" Trousers #" + (indexTrousers + 1));
        shoesName.SetText(" Shoes #" + (indexShoes + 1));
    }

    private void ColorItem(string partName, string color) {
        setNewItemColor(partName, "#" + color);
    }

    public void saveCharacter() {

        Game g = Helper.ReadGameFile();

        int numSaves = g.enterprises.Length;
        numSaves++;

        if (numSaves > 3) {
            numSaves = 3;
        }

        Enterprise[] eps = new Enterprise[numSaves];

        for (int i = 0; i < g.enterprises.Length; i++) {
            if (g.enterprises[i] != null) {
                eps[i] = g.enterprises[i];
            }
        }

		//todo: inserir en un json els valors inicials per defecte
        Enterprise e = new Enterprise();
        e.enterpriseId = LevelManager.gameId;
        e.active = true;

        e.enterpriseName = companyName.text;
        e.enterpriseType = "Freelance";

        e.currentMoney = 1000;
        e.reputation = 1;
        e.popularity = 1;
        e.administrativeCosts = 60;
        e.laboralRisks = 160;
		e.taxes = 600;
		e.licenses = 120;
        e.loans = null;

        e.office = new Office {
            monthlyCost = 300,
            internetCost = 60,
            basicElectricityCost = 50,
            insuranceCost = 30,
            happinessEffect = 4,
            equipmentObjects = null
        };

		e.lastIdWorker = 1;
		string hairName = Regex.Split(hairList[indexHair].name, "_idle")[0];
		string shirtName = Regex.Split(shirtList[indexShirt].name, "_idle")[0];
		string trousersName = Regex.Split(trousersList[indexTrousers].name, "_idle")[0];
		string shoesName = Regex.Split(shoesList[indexShoes].name, "_idle")[0];

		e.workers = new Character[] {
            new Character {
				idWorker = 1,
                smoothMoveSpeed = 2,
                bodyParts = new BodyParts[] {
                    new BodyParts {
						nameSprite = hairName,
						color = Helper.convertColor(hairViewer.color),
                        part = BodyPartsConstants.HAIR
                    },
                    new BodyParts {
                        nameSprite = "head",
                        color = Helper.convertColor(skinColor),
                        part = BodyPartsConstants.HEAD
                    },
                    new BodyParts {
                        nameSprite = "body",
                        color = Helper.convertColor(skinColor),
                        part = BodyPartsConstants.BODY
                    },
                    new BodyParts {
                        nameSprite = shirtName,
                        color =Helper.convertColor(shirtViewer.color),
                        part = BodyPartsConstants.SHIRT
                    },
                    new BodyParts {
                        nameSprite = "legs",
                        color = Helper.convertColor(skinColor),
                        part = BodyPartsConstants.LEGS
                    },
					new BodyParts {
						nameSprite = trousersName,
						color = Helper.convertColor(trousersViewer.color),
						part = BodyPartsConstants.TROUSERS
					},
					new BodyParts {
                        nameSprite = shoesName,
                        color = Helper.convertColor(shoesViewer.color),
                        part = BodyPartsConstants.FOOT
                    }
                },
                skills = new CharacterSkill[]{ },
                languages = new CharacterLanguage[]{ },
                nameCharacter = playerName.text,
                motivation = "Tú si que vales!",
				cotization = 350,
				insurance = 40,
                stress = 5,
                hungry = 5,
                energy = 5,
                happiness = 5,
				xPosition = 11.5f,
				yPosition = 1.5f
			}
        };

		e.proposalWorkers = new Character[] { };

		e.projects = new ProjectsList()
		{
			proposalProjects = new Project[] { },
			doingProjects = new Project[] { },
			doneProjects = new Project[] { }
		};

        e.secondsPlayed = 0;

        e.foundationDay = System.DateTime.Now.Day;
        e.foundationMonth = System.DateTime.Now.Month;
        e.foundationYear = System.DateTime.Now.Year;

        e.actualDay = 1;
        e.actualMonth = 1;
        e.actualYear = 2019;

        e.savedDay = System.DateTime.Now.Day;
        e.savedMonth = System.DateTime.Now.Month;
        e.savedYear = System.DateTime.Now.Year;
        e.savedHour = System.DateTime.Now.Hour;
        e.savedMinute = System.DateTime.Now.Minute;

        eps[LevelManager.gameId-1] = e;

        g.enterprises = eps;

        string json = JsonUtility.ToJson(g, true);
        Debug.Log(json);

        using (StreamWriter sw = File.CreateText(Helper.pathGame)) {
            sw.Write(json);
        }

        LevelManager.enterprise = e;
        LevelManager.LoadGame();

    }

    public void RandomCharacter() {

        // clothes
        if (randomClothes) {
            setNewHair(Random.Range(0, hairList.Count));
            setNewShirt(Random.Range(0, shirtList.Count));
            setNewTrousers(Random.Range(0, trousersList.Count));
            setNewShoes(Random.Range(0, shoesList.Count));
        }

        // color
        if (randomColors) {
            setNewItemColor("skin", getRandomColorFromList(skinColors));
            setNewHairColor(getRandomColorFromList(hairColors));
            setNewShirtColor(getRandomColorFromList(shirtColors));
            setNewTrousersColor(getRandomColorFromList(trousersColors));
            setNewShoesColor(getRandomColorFromList(shoesColors));
        }
    }

    private string getRandomColorFromList(List<string> colors) {

        int min = 0;
        int max = colors.Count;

        return colors[Random.Range(min, max)];

    }

    public void previousSelection() {
        selection_one.SetActive(true);
        selection_two.SetActive(false);
    }

    public void nextSelection() {
        selection_one.SetActive(false);
        selection_two.SetActive(true);
    }

    //****************************
    // UNIVERSAL FUNCTIONS TO CHANGE HAIR, SHIRT, TROUSERS, SHOES AND COLOR

    // name = hair || shirt || trousers || shoes
    private void setPreviousItem(string name, int actualIndex, List<Sprite> listOfSprites, Image itemViewer) {

        actualIndex--;

        switch (name) {
            case "hair":
                if (actualIndex < 0) {
                    actualIndex = hairList.Count - 1;
                }
                indexHair = actualIndex;
                setNewHair(indexHair);
                break;
            case "shirt":
                if (actualIndex < 0) {
                    actualIndex = shirtList.Count - 1;
                }
                indexShirt = actualIndex;
                setNewShirt(indexShirt);
                break;
            case "trousers":
                if (actualIndex < 0) {
                    actualIndex = trousersList.Count - 1;
                }
                indexTrousers = actualIndex;
                setNewTrousers(indexTrousers);
                break;
            case "shoes":
                if (actualIndex < 0) {
                    actualIndex = shoesList.Count - 1;
                }
                indexShoes = actualIndex;
                setNewShoes(indexShoes);
                break;
            default:
                break;
        }

    }

    // name = hair || shirt || trousers || shoes
    private void setNextItem(string name, int actualIndex, List<Sprite> listOfSprites, Image itemViewer) {

        actualIndex++;

        if (actualIndex == listOfSprites.Count) {
            actualIndex = 0;
        }

        switch (name) {
            case "hair":
                indexHair = actualIndex;
                setNewHair(indexHair);
                break;
            case "shirt":
                indexShirt = actualIndex;
                setNewShirt(indexShirt);
                break;
            case "trousers":
                indexTrousers = actualIndex;
                setNewTrousers(indexTrousers);
                break;
            case "shoes":
                indexShoes = actualIndex;
                setNewShoes(indexShoes);
                break;
            default:
                break;
        }

    }

    // name = hair || shirt || trousers || shoes
    private void setNewItemColor(string name, string color) {

        if (ColorUtility.TryParseHtmlString(color, out Color newColor)) {

            switch (name) {
                case "skin":

                    head.color = newColor;
                    body.color = newColor;
                    legs.color = newColor;

                    skinColor = newColor;
                    break;
                case "hair":
                    hairViewer.color = newColor;
                    break;
                case "shirt":
                    shirtViewer.color = newColor;
                    break;
                case "trousers":
                    trousersViewer.color = newColor;
                    break;
                case "shoes":
                    shoesViewer.color = newColor;
                    break;
                default:
                    break;
            }
        }
    }

    //****************************
    // HAIR

    public void setPreviousHair() {

        setPreviousItem("hair", indexHair, hairList, hairViewer);

    }

    public void setNextHair() {

        setNextItem("hair", indexHair, hairList, hairViewer);

    }

    public void setNewHairColor(string color) {

        setNewItemColor("hair", color);

    }

    private void setNewHair(int index) {
        hairViewer.overrideSprite = hairList[index];
        hairName.SetText(" Hair #" + (index + 1));
    }

    //****************************
    // SHIRT

    public void setPreviousShirt() {

        setPreviousItem("shirt", indexShirt, shirtList, shirtViewer);

    }

    public void setNextShirt() {

        setNextItem("shirt", indexShirt, shirtList, shirtViewer);

    }

    public void setNewShirtColor(string color) {

        setNewItemColor("shirt", color);

    }

    private void setNewShirt(int index) {
        shirtViewer.overrideSprite = shirtList[index];
        shirtName.SetText(" Shirt #" + (index + 1));
    }

    //****************************
    // TROUSERS

    public void setPreviousTrousers() {

        setPreviousItem("trousers", indexTrousers, trousersList, trousersViewer);

    }

    public void setNextTrousers() {

        setNextItem("trousers", indexTrousers, trousersList, trousersViewer);

    }

    public void setNewTrousersColor(string color) {

        setNewItemColor("trousers", color);

    }

    private void setNewTrousers(int index) {
        trousersViewer.overrideSprite = trousersList[index];
        trousersName.SetText(" Trousers #" + (index + 1));
    }

    //****************************
    // SHOES

    public void setPreviousShoes() {

        setPreviousItem("shoes", indexShoes, shoesList, shoesViewer);

    }

    public void setNextShoes() {

        setNextItem("shoes", indexShoes, shoesList, shoesViewer);

    }

    public void setNewShoesColor(string color) {

        setNewItemColor("shoes", color);

    }

    private void setNewShoes(int index) {
        shoesViewer.overrideSprite = shoesList[index];
        shoesName.SetText(" Shoes #" + (index + 1));
    }

}
