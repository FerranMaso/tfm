﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{
	public Slider musicSlider;
	public Slider soundSlider;

	public GameObject Screen1;
	public GameObject Screen2;
	public GameObject Screen3;
	public GameObject ScreenDelete;

	public AudioSource click;
	public AudioSource bin;
	public AudioSource deleteAudio;

	public Text title;

	Game g = null;
    // Start is called before the first frame update
    void Start()
    {
		g = Helper.ReadGameFile();
		if(g == null || g.enterprises == null)
		{
			Debug.Log("No games found");
		}
		else
		{
			Debug.Log("Games found");
			
		}
		LoadEnterprises();
		LevelManager.music = PlayerPrefs.GetFloat("Music", 50);
		LevelManager.sound = PlayerPrefs.GetFloat("Sound", 50);
	}

    // Update is called once per frame
    void Update()
    {
		if(Screen2.activeSelf)
		{
			if( musicSlider.value != LevelManager.music) {
				LevelManager.music = musicSlider.value;
			}

			if( soundSlider.value != LevelManager.sound) {
				LevelManager.sound = soundSlider.value;
				click.volume = LevelManager.sound/100f;
				bin.volume = LevelManager.sound/100f;
				deleteAudio.volume = LevelManager.sound/100f;
			}
			
		}
    }

	private void LoadEnterprises()
	{
		int i = 1;
		foreach (Enterprise e in g.enterprises)
		{
			if (e == null || !e.active)
			{
				GameObject game = GameObject.Find("Game" + i);
				if (game != null)
				{
					Transform[] trs = game.GetComponentsInChildren<Transform>(true);
					foreach (Transform t in trs)
					{
						if (t.name == "WithData")
						{
							t.gameObject.SetActive(false);
						}
						else if (t.name == "WithoutData")
						{
							t.gameObject.SetActive(true);
						}
					}
				}
			}
			else
			{
				GameObject game = GameObject.Find("Game" + e.enterpriseId);
				if (game != null)
				{
					Transform[] trs = game.GetComponentsInChildren<Transform>(true);
					Transform gameContainer = null;
					foreach (Transform t in trs)
					{
						if (t.name == "WithData")
						{
							t.gameObject.SetActive(true);
							gameContainer = t;
						}
						else if (t.name == "WithoutData")
						{
							t.gameObject.SetActive(false);
						}
					}
					if (gameContainer != null)
					{
						Transform timePlayed = gameContainer.Find("TimePlayed");
						Transform companyName = gameContainer.Find("CompanyName");
						Transform money = gameContainer.Find("Money");
						Transform date = gameContainer.Find("Date");

						timePlayed.GetComponent<Text>().text = "" + Helper.SecondsToFullString(e.secondsPlayed);
						companyName.GetComponent<Text>().text = e.enterpriseName;
						money.GetComponent<Text>().text = e.currentMoney.ToString("n2") + "€";
						date.GetComponent<Text>().text = e.savedDay + "/" + e.savedMonth + "/" + e.savedYear + " " + e.savedHour + ":" + e.savedMinute;
					}
				}
			}
			i++;
		}
	}

	public void LoadGame(int id)
	{
		if (g == null || g.enterprises == null)
		{
			LevelManager.gameId = id;
			LevelManager.enterprise = null;
			LevelManager.LoadCharacterCreation();
		}
		else
		{
			Enterprise selectedEnterprise = null;

			foreach (Enterprise e in g.enterprises)
			{
				if (e!=null && e.enterpriseId == id)
				{
					selectedEnterprise = e;
				}
			}
			if(selectedEnterprise == null || !selectedEnterprise.active)
			{
				LevelManager.gameId = id;
				LevelManager.enterprise = null;
				LevelManager.LoadCharacterCreation();
			}
			else
			{
				LevelManager.gameId = id;
				LevelManager.enterprise = selectedEnterprise;
				LevelManager.LoadGame();
			}
		}

	}

	public void DeleteGame(int id)
	{
		Transform gameDelete = Helper.FindDeepChild(ScreenDelete.transform, "GameDelete" + id);
		Transform withData = Helper.FindDeepChild(gameDelete, "WithData");
		Transform delete = Helper.FindDeepChild(gameDelete, "Delete");
		if (withData.gameObject.activeSelf)
		{
			withData.gameObject.SetActive(false);
			delete.gameObject.SetActive(true);
		}
	}

	public void DeleteDefinitlyGame(int id)
	{
		bin.Play();
		g.enterprises[id - 1] = null;
		Helper.WriteGames(null, id);
		Transform gameDelete = Helper.FindDeepChild(ScreenDelete.transform, "GameDelete" + id);
		Transform withoutData = Helper.FindDeepChild(gameDelete, "WithoutData");
		Transform delete = Helper.FindDeepChild(gameDelete, "Delete");

		withoutData.gameObject.SetActive(true);
		delete.gameObject.SetActive(false);
	}

	public void UnDeleteGame(int id)
	{
		Transform gameDelete = Helper.FindDeepChild(ScreenDelete.transform, "GameDelete" + id);
		Transform withData = Helper.FindDeepChild(gameDelete, "WithData");
		Transform delete = Helper.FindDeepChild(gameDelete, "Delete");
		if (delete.gameObject.activeSelf)
		{
			withData.gameObject.SetActive(true);
			delete.gameObject.SetActive(false);
		}
	}

	public void GoOptions()
	{
		click.Play();
		Screen1.SetActive(false);
		Screen2.SetActive(true);
		title.text = "Opciones";
		musicSlider.value = LevelManager.music;
		soundSlider.value = LevelManager.sound;
	}

	public void GoCredits()
	{
		click.Play();
		Screen1.SetActive(false);
		Screen3.SetActive(true);
		title.text = "Créditos";
	}

	public void Back()
	{
		title.text = "Deventerprise";
		Screen1.SetActive(true);
		LoadEnterprises();
		Screen2.SetActive(false);
		Screen3.SetActive(false);
		ScreenDelete.SetActive(false);
		click.Play();

		PlayerPrefs.SetFloat("Music", LevelManager.music);
		PlayerPrefs.SetFloat("Float", LevelManager.sound);
	}

	public void DeleteGame()
	{
		deleteAudio.Play();
		ScreenDelete.SetActive(true);
		Screen1.SetActive(false);
		int i = 1;
		foreach (Enterprise e in g.enterprises)
		{
			if (e == null || !e.active)
			{
				GameObject gameDelete = GameObject.Find("GameDelete" + i);
				if (gameDelete != null)
				{
					Transform[] trs = gameDelete.GetComponentsInChildren<Transform>(true);
					foreach (Transform t in trs)
					{
						if (t.name == "WithData")
						{
							t.gameObject.SetActive(false);
						}
						else if (t.name == "WithoutData")
						{
							t.gameObject.SetActive(true);
						}
					}
				}
			}
			else
			{
				GameObject gameDelete = GameObject.Find("GameDelete" + e.enterpriseId);
				if (gameDelete != null)
				{
					Transform[] trs = gameDelete.GetComponentsInChildren<Transform>(true);
					Transform gameContainer = null;
					foreach (Transform t in trs)
					{
						if (t.name == "WithData")
						{
							t.gameObject.SetActive(true);
							gameContainer = t;
						}
						else if (t.name == "WithoutData")
						{
							t.gameObject.SetActive(false);
						}
					}
					if (gameContainer != null)
					{
						Transform timePlayed = gameContainer.Find("TimePlayed");
						Transform companyName = gameContainer.Find("CompanyName");
						Transform money = gameContainer.Find("Money");
						Transform date = gameContainer.Find("Date");

						timePlayed.GetComponent<Text>().text = "" + Helper.SecondsToFullString(e.secondsPlayed);
						companyName.GetComponent<Text>().text = e.enterpriseName;
						money.GetComponent<Text>().text = e.currentMoney.ToString("n2") + "€";
						date.GetComponent<Text>().text = e.savedDay + "/" + e.savedMonth + "/" + e.savedYear + " " + e.savedHour + ":" + e.savedMinute;
					}
				}
			}
			i++;
		}
	}

	private void RemoveEnterprise(int idEnterprise)
	{
		Enterprise[] supportArray = new Enterprise[g.enterprises.Length - 1]; int j = 0;
		int position = ExistEnterprise(idEnterprise);
		if(position != 1)
		{
			for (int i = 0; i < g.enterprises.Length; i++)
			{
				if (idEnterprise != g.enterprises[i].enterpriseId)
				{
					supportArray[j] = g.enterprises[i];
					j++;
				}
			}
			g.enterprises = supportArray;
		}
	}

	private int ExistEnterprise(int id)
	{
		int position = -1;

		for (int i = 0; i < g.enterprises.Length; i++)
		{
			if(g.enterprises[i].enterpriseId == id)
			{
				position = i;
			}
		}

		return position;
	}
}
