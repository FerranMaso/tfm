﻿using UnityEngine.SceneManagement;

public class LevelManager
{
	public static int gameId;
	public static Enterprise enterprise;

	public static float music;
	public static float sound;

	public const string MENU = "1_0_Menu";
	public const string CHARACTER_CREATION = "1_1_CharacterSelect";
	public const string GAME = "2_0_Game";

	public static void LoadMenu()
	{
		LoadScene(MENU);
	} 

	public static void LoadCharacterCreation()
	{
		LoadScene(CHARACTER_CREATION); 
	}

	public static void LoadGame()
	{
		LoadScene(GAME);
	}

	static void LoadScene(string sceneName)
	{
		SceneManager.LoadScene(sceneName);
	}
}
