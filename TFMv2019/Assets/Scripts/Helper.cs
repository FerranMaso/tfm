﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Threading;
using UnityEngine;



public class Helper
{
	//todo: fer privat i inserir una funció en el helper dels llocs on s'utilitzi
	public static string pathGame = Path.Combine(Application.streamingAssetsPath, "Game.json");
	private static string pathProjects = Path.Combine(Application.streamingAssetsPath, "ProjectsDefault.json");
	private static string pathWorkers = Path.Combine(Application.streamingAssetsPath, "WorkersDefault.json");

	public static Vector3 IsometricToReal(Vector3 iso, Vector3 desp)
	{
		float xIso = iso.x;
		float yIso = iso.y;

		float xReal = (xIso * 0.5f) + (yIso * (-0.5f)) + desp.x;
		float yReal = (xIso * 0.25f) + (yIso * 0.25f) + desp.y;

		return new Vector3(xReal, yReal, 0);
	}

	public static Vector3 RealToIsometric(Vector3 real, Vector3 desp, bool arr)
	{
		float xReal = real.x;
		float yReal = real.y;

		float xIso = xReal + 2 * yReal - desp.x - 2 * desp.y;
		float yIso = -xReal + 2 * yReal + desp.x - 2 * desp.y;
		if(arr)
		{
			return new Vector3((int)xIso, (int)yIso, 0);
		}
		else
		{
			return new Vector3(xIso, yIso, 0);
		}
	}

	public static string DateToString(DateTime dateValue)
	{
		int dayOfWeek = (int)dateValue.DayOfWeek-1;
		if (dayOfWeek < 0) dayOfWeek = TimeReferences.daysNumber - 1; 

		return TimeReferences.DaysAbr[dayOfWeek] + " " + dateValue.Day + "/" + dateValue.Month + "/" + dateValue.Year;
	}

	public static void createFile()
	{

		/*Game mainGame = new Game();
		mainGame.players = new List<Player>();
		Player player = new Player();

		GameObject playerGO = GameObject.FindGameObjectWithTag("Player");

		player.playerName = "Monroe";
		player.level = 1;

		if (playerGO != null)
		{
			player.xPosition = playerGO.transform.localPosition.x;
			player.yPosition = playerGO.transform.localPosition.y;
		}
		else
		{
			player.xPosition = 0;
			player.yPosition = 0;
		}

		mainGame.players.Add(player);
		mainGame.actualScene = SceneManager.GetActiveScene().buildIndex;
		mainGame.timePlayed = 14;

		string mainGameJSON = JsonUtility.ToJson(mainGame, true); // true per donar format json

		// using serveix per crear IDisposable object, es guarda i es tancar que acaba el codi
		using (StreamWriter sw = File.CreateText(path))
		{
			sw.Write(mainGameJSON);
		}*/

	}

	public static Game ReadGameFile()
	{

		string jsonData = "";

		using (StreamReader sr = File.OpenText(pathGame))
		{
			jsonData = sr.ReadToEnd();
		}

		//Destroy(GameObject.FindWithTag("Game"));

		Game game = JsonUtility.FromJson<Game>(jsonData);
		return game;
	}

	public static ProjectsDefault ReadProjectsFile()
	{

		string jsonData = "";

		using (StreamReader sr = File.OpenText(pathProjects))
		{
			jsonData = sr.ReadToEnd();
		}
		//Debug.Log(jsonData);


		ProjectsDefault projectsDefault = JsonUtility.FromJson<ProjectsDefault>(jsonData);
		return projectsDefault;
	}

	public static WorkersDefault ReadWorkersDefault()
	{

		string jsonData = "";

		using (StreamReader sr = File.OpenText(pathWorkers))
		{
			jsonData = sr.ReadToEnd();
		}

		WorkersDefault projectsDefault = JsonUtility.FromJson<WorkersDefault>(jsonData);
		return projectsDefault;
	}

	public static void WriteGames(Enterprise enterprise)
	{
		Debug.Log("Saving the game");



		Game g = ReadGameFile();

		int index = 0;
		int count = 0;

		foreach (Enterprise e in g.enterprises)
		{
			if (e.enterpriseId == LevelManager.gameId)
			{
				index = count;
			}
			count++;
		}

		Debug.Log(index);
		g.enterprises[index] = enterprise;

		using (StreamWriter sw = File.CreateText(Helper.pathGame))
		{
			sw.Write(JsonUtility.ToJson(g, true));
		}
	}

	public static void WriteGames(Enterprise enterprise, int idEnterprise)
	{
		Debug.Log("Saving the game");



		Game g = ReadGameFile();

		int index = 0;
		int count = 0;

		foreach (Enterprise e in g.enterprises)
		{
			if (e.enterpriseId == idEnterprise)
			{
				index = count;
			}
			count++;
		}

		Debug.Log(index);
		g.enterprises[index] = enterprise;

		using (StreamWriter sw = File.CreateText(Helper.pathGame))
		{
			sw.Write(JsonUtility.ToJson(g, true));
		}
	}


	public void DeleteFile()
	{
		if (File.Exists(pathGame))
		{
			File.Delete(pathGame);
		}
	}

	public static string SecondsToFullString(int seconds)
	{
		TimeSpan time = TimeSpan.FromSeconds(seconds);

		//here backslash is must to tell that colon is
		//not the part of format, it just a character that we want in output
		return string.Format("{0:hh\\:mm\\:ss}", time);
	}

	public static Transform FindDeepChild(Transform aParent, string aName)
	{
		Queue<Transform> queue = new Queue<Transform>();
		queue.Enqueue(aParent);
		while (queue.Count > 0)
		{
			var c = queue.Dequeue();
			if (c.name == aName)
				return c;
			foreach (Transform t in c)
				queue.Enqueue(t);
		}
		return null;
	}

	public static void Resize<T>(ref T[] array, int newSize)
	{
		if (newSize < 0)
		{
			throw new Exception("cannot resize under 0");
		}
		T[] sourceArray = array;
		if (sourceArray == null)
		{
			array = new T[newSize];
		}
		else if (sourceArray.Length != newSize)
		{
			T[] destinationArray = new T[newSize];
			Array.Copy(sourceArray, 0, destinationArray, 0, (sourceArray.Length > newSize) ? newSize : sourceArray.Length);
			array = destinationArray;
		}
	}

	public static string convertColor(Color color)
	{
		return "#" + ColorUtility.ToHtmlStringRGB(color);
	}

}
